option(
	'build-as-subproject',
	type: 'boolean',
	value: false,
	description : 'Build only static library and do not install headers'
)


