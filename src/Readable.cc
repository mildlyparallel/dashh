#include <fstream>
#include <iostream>

#include "Readable.hh"

#include "Argument.hh"

namespace dashh
{

Readable::Readable(std::string &output)
: m_output(output)
{ }

Readable::~Readable()
{ }

Parser::Result Readable::parse(
	const std::string &,
	const std::string &value,
	const Argument *
) const
{
	std::ifstream in(value);
	if (!in.good())
		return Parser::Error;

	m_output = value;
	return Parser::Parsed;
}

Parser *Readable::clone() const
{
	return new Readable(*this);
}

Readable as_readable(std::string &output)
{
	return Readable(output);
}

void Readable::print_error_message(
	const std::string &key,
	const std::string &value,
	const Argument *
) const {
	std::cerr << "The file " << value << " (from " << key << " argument) is not readable." << std::endl;
}

} /* dashh */ 
