#include "TreeNode.hh"

#include <cassert>

namespace dashh
{

TreeNode::TreeNode()
: m_parent(nullptr)
, m_sibling(nullptr)
, m_child(nullptr)
, m_last(nullptr)
{ }

TreeNode::TreeNode(const TreeNode &other)
: m_parent(nullptr)
, m_sibling(nullptr)
, m_child(nullptr)
, m_last(nullptr)
{
	for (TreeNode *n = other.m_child; n; n = n->m_sibling)
		add(n->clone());
}

TreeNode::TreeNode(TreeNode &&other) noexcept
: m_parent(nullptr)
, m_sibling(nullptr)
, m_child(nullptr)
, m_last(nullptr)
{
	TreeNode *n = other.m_child;
	while (n) {
		TreeNode *t = n->m_sibling;
		add(n);
		n = t;
	}

	if (other.m_parent) {
		TreeNode **p = &(other.m_parent->m_child);

		while (*p && *p != &other)
			p = &((*p)->m_sibling);

		if (*p)
			*p = this;
	}

	m_parent = other.m_parent;
	m_sibling = other.m_sibling;
	
	other.m_child = nullptr;
	other.m_last = nullptr;
	other.m_parent = nullptr;
	other.m_sibling = nullptr;
}

TreeNode::~TreeNode()
{ }

void TreeNode::add(TreeNode *node)
{
	assert(node);
	node->m_parent = nullptr;
	node->m_sibling = nullptr;

	node->m_parent = this;

	if (!m_child) {
		m_child = node;
		m_last = node;
		return;
	}

	m_last->m_sibling = node;
	m_last = node;
}

TreeNode *TreeNode::sibling() const
{
	return m_sibling;
}

TreeNode *TreeNode::child() const
{
	return m_child;
}

TreeNode *TreeNode::parent() const
{ 
	return m_parent;
}

} /*  dashh */ 
