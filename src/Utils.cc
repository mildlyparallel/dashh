#include <cassert>
#include <limits>
#include "Utils.hh"

namespace dashh
{
namespace Utils
{

std::string join(const std::vector<std::string> &v, const std::string &s)
{
	bool sep = false;

	std::string ret; 

	for (auto &i : v) {
		if (sep)
			ret += s;
		ret += i;
		sep = true;
	}

	return ret;
}

std::string nspaces(size_t n)
{
	std::string s;
	for (size_t i = 0; i < n; ++i)
		s += " ";

	return s;
}

void for_line(
	const std::string &text,
	std::function<void (const std::string &line)> callback
) {
	size_t end = 0;
	size_t bgn = 0;

	for (size_t i = 0; i < text.length(); ++i) {
		if (text[i] != '\n')
			continue;

		end = i;

		callback(text.substr(bgn, end - bgn));

		bgn = i + 1;
	}

	callback(text.substr(bgn));
}

std::vector<std::string> split(
	const std::string &s,
	const std::string &delims
) {
	std::vector<std::string> output;
	std::string item;

	for (auto &c : s) {
		if (delims.find(c) == std::string::npos) {
			item += c;
		} else if (!item.empty()) {
			output.push_back(item);
			item.clear();
		}
	}

	if (!item.empty())
		output.push_back(item);

	return output;
}

std::string trace_to_path(const std::vector<std::string> &trace)
{
	assert(!trace.empty());

	auto s = split(trace[0], "/");
	std::string p = s.back();

	for (size_t i = 1; i < trace.size(); ++i) {
		p += " ";
		p += trace[i];
	}

	return p;
}

unsigned stou(const std::string &s, size_t *pos, int b)
{
	unsigned long ul = std::stoul(s, pos, b);

	if (ul > std::numeric_limits<unsigned>::max())
		throw std::out_of_range("stou");

	return ul;
}

} /* Utils */ 
} /* dashh */ 
