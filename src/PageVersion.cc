#include <cassert>
#include <cstdio>

#include "Argument.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Command.hh"
#include "PageVersion.hh"
#include "Utils.hh"

namespace dashh
{
PageVersion::PageVersion()
{ }

PageVersion::~PageVersion()
{ }

void PageVersion::add(const Command *command)
{
	if (!command->version().empty()) {
		m_version = command->version();
		return;
	}

	if (command->parent())
		command->parent()->dispatch(this);
}

void PageVersion::add(const Flag *flag)
{
	if (flag->parent())
		flag->parent()->dispatch(this);
}

void PageVersion::add(const Option *option)
{
	if (option->parent())
		option->parent()->dispatch(this);
}

void PageVersion::add(const Positional *)
{  }

void PageVersion::add(const Text *)
{  }

void PageVersion::add(const Usage *)
{  }

void PageVersion::add(const KeyValue *)
{  }


Page *PageVersion::clone() const
{
	return new PageVersion(*this);
}

void PageVersion::build(
	const Argument *argument,
	const std::vector<std::string> &
) { 
	argument->dispatch(this);
}

void PageVersion::print(const std::string &, const std::string &)
{
	fprintf(stdout, "%s\n", m_version.c_str());
}

void PageVersion::exit() const
{
	std::exit(EXIT_SUCCESS);
}

} /*  dashh */ 
