#include <cassert>

#include "Argument.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Command.hh"
#include "PageMissingValue.hh"

#include "Utils.hh"

namespace dashh
{

PageMissingValue::PageMissingValue()
{ }

PageMissingValue::~PageMissingValue()
{ }

Page *PageMissingValue::clone() const
{
	return new PageMissingValue(*this);
}

void PageMissingValue::build(
		const Argument *,
		const std::vector<std::string> &trace
) { 
	assert(!trace.empty());
	m_cmd = Utils::trace_to_path(trace);
}

void PageMissingValue::print(const std::string &key, const std::string &)
{
	fprintf(stderr, "%s: option \'%s\' requires a value!\n", m_cmd.c_str(), key.c_str());
}

void PageMissingValue::exit() const
{
	std::exit(EXIT_FAILURE);
}

} /*  dashh */ 
