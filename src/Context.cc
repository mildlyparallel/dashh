#include <cassert>
#include "Context.hh"

namespace dashh
{

Context::Context()
: m_position(0)
, m_finilized(false)
{ }

Context::Context(int argc, char const *argv[])
: m_position(0)
, m_finilized(false)
{
	for (int i = 0; i < argc; ++i) {
		m_data.push_back(argv[i]);
	}
}

Context::~Context()
{ }

const std::string &Context::current() const
{
	assert(!end());
	return m_data.at(m_position);
}

const std::string &Context::next() const
{
	assert(!end());
	assert(!last());
	return m_data.at(m_position + 1);
}

void Context::advance()
{
	if (!end())
		m_position++;
}

void Context::finilize()
{
	m_finilized = true;
}

size_t Context::position() const
{
	return m_position;
}

bool Context::last() const
{
	if (m_finilized)
		return false;

	return (m_position + 1) == m_data.size();
}

bool Context::end() const
{
	if (m_finilized)
		return true;

	return m_position >= m_data.size();
}

} /* dashh */ 
