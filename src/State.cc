#include <algorithm>

#include "State.hh"

#include "Argument.hh"

namespace dashh
{

State::State(std::string &output, const std::vector<std::string> &states)
: m_output(output)
, m_states(states)
{
	for (auto &s : m_states) {
		std::transform(s.begin(), s.end(), s.begin(), ::tolower);
	}
}

State::~State()
{ }

Parser::Result State::parse(
	const std::string &,
	const std::string &value,
	const Argument *
) const
{
	std::string v = value;
	std::transform(v.begin(), v.end(), v.begin(), ::tolower);

	for (const auto &s : m_states) {
		if (s == v) {
			m_output = value;
			return Parser::Parsed;
		}
	}

	return Parser::Error;
}

Parser *State::clone() const
{
	return new State(*this);
}

State as_state(std::string &output, const std::vector<std::string> &states)
{
	return State(output, states);
}

} /* dashh */ 
