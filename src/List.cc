#include <algorithm>
#include <cassert>


#include "List.hh"

#include "Argument.hh"

namespace dashh
{

List::List(Parser &&parser, const std::string &delim)
: Parser(std::move(parser))
, m_delim(delim)
{ }

List::List(const Parser &parser, const std::string &delim)
: Parser(parser)
, m_delim(delim)
{ }

List::~List()
{ }

Parser::Result List::parse(
	const std::string &key,
	const std::string &value,
	const Argument *a
) const {
	for (const auto &v : Utils::split(value, m_delim)) {
		Parser::Result r = m_parser->parse(key, v, a);
		if (r != Parser::Parsed)
			return r;
	}

	return Parser::Parsed;
}

Parser *List::clone() const
{
	return new List(*this);
}

List as_list(const Parser &parser, const std::string &delim)
{
	return List(parser, delim);
}

List as_list(std::vector<std::string> &output, const std::string &delim)
{
	return List(Parser(output), delim);
}

List as_list(std::vector<bool> &output, const std::string &delim)
{
	return List(Parser(output), delim);
}

} /* dashh */ 

