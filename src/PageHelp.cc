#include <cassert>
#include <iostream>

#include "Argument.hh"
#include "KeyValue.hh"
#include "Flag.hh"
#include "Usage.hh"
#include "Text.hh"
#include "Option.hh"
#include "Positional.hh"
#include "Command.hh"
#include "PageHelp.hh"
#include "Utils.hh"

namespace dashh
{
PageHelp::PageHelp()
: m_have_subcommands(false)
, m_have_arguments(false)
, m_have_required_arguments(false)
{ }

PageHelp::~PageHelp()
{ }

void PageHelp::add(const Command *command)
{
	// First function call is from parent command
	static bool is_parent = true;

	if (is_parent) {
		is_parent = false;

		m_usage = command->usage();
		for (auto child = command->child(); child; child = child->sibling())
			child->dispatch(this);

		return;
	}

	// Following function calls is from children subcommands
	if (command->type().hidden())
		return;

	m_have_subcommands = true;

	KeyValue kv(pattern(command), command->description());
	add(&kv);
}

void PageHelp::add(const Flag *flag)
{
	if (flag->type().hidden())
		return;

	m_have_arguments = true;

	KeyValue c(pattern(flag), flag->description());
	add(&c);
}

void PageHelp::add(const Option *option)
{
	if (option->type().hidden())
		return;

	m_have_arguments = true;

	if (option->type().required())
		m_have_required_arguments = true;

	KeyValue kv(pattern(option), option->description());
	add(&kv);
}

void PageHelp::add(const Positional *positional)
{
	if (positional->type().hidden())
		return;

	m_have_arguments = true;

	if (positional->type().required())
		m_have_required_arguments = true;

	KeyValue kv(pattern(positional), positional->description());
	add(&kv);
}

void PageHelp::add(const Usage *usage)
{ 
	m_body += std::string(textIndent, ' ') + m_cmd + " " + usage->get() + "\n";
}

void PageHelp::add(const Text *text)
{
	std::string wrapped = wrap(text->text(), text->width());
	Utils::for_line(wrapped, [&](const std::string &line) {
		m_body += std::string(text->indent(), ' ') + line + "\n";
	});
}

void PageHelp::add(const KeyValue *columns)
{
	m_body += std::string(columns->keyIndent(), ' ') + columns->col1();

	size_t offset;
	if (columns->col1().length() > columns->keyWidth()) {
		offset = columns->keyIndent() + columns->keyWidth() + columns->valIndent();
		m_body += "\n";
	} else {
		offset = (columns->keyWidth() - columns->col1().length()) + columns->valIndent();
	}

	std::string wrapped = wrap(columns->col2(), columns->valWidth());
	Utils::for_line(wrapped, [&](const std::string &line) {
		m_body += std::string(offset, ' ') + line + "\n";
		offset = columns->keyIndent() + columns->keyWidth() + columns->valIndent();
	});
}

Page *PageHelp::clone() const
{
	return new PageHelp(*this);
}

void PageHelp::build(
		const Argument *argument,
		const std::vector<std::string> &trace
) { 
	assert(!trace.empty());

	m_cmd = Utils::trace_to_path(trace);

	assert(argument->parent());
	argument->parent()->dispatch(this);
}

void PageHelp::print(const std::string &, const std::string &)
{
	fprintf(stdout, "%s", m_body.c_str());
}

void PageHelp::exit() const
{
	std::exit(EXIT_SUCCESS);
}

} /*  dashh */ 
