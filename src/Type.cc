#include "Type.hh"

#include <cstdint>

namespace dashh
{
	
Type::Type(Type::type_e type)
: m_type(type)
{ }

Type::Type(uint64_t type)
: m_type(static_cast<type_e>(type))
{ }

Type::~Type()
{ }

bool Type::none() const
{
	return m_type == 0;
}

bool Type::required() const
{
	return m_type & Required;
}

bool Type::multiple() const
{
	return m_type & Multiple;
}

bool Type::hidden() const
{
	return m_type & Hidden;
}

bool Type::greedy() const
{
	return m_type & Greedy;
}

Type Type::operator| (const Type &other) const
{
	return 
		static_cast<uint64_t>(m_type) |
		static_cast<uint64_t>(other.m_type);
}

bool Type::operator==(const Type &other) const
{
	return m_type == other.m_type;
}

Type operator| (Type::type_e a, Type::type_e b)
{
	return Type(a) | Type(b);
}

} /* dashh */ 

