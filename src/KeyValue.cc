#include "KeyValue.hh"
#include "Utils.hh"
#include "Dashh.hh"
#include "Page.hh"

namespace dashh
{

KeyValue::KeyValue(
	const std::string &col1,
	const std::string &col2,
	size_t keyIndent,
	size_t keyWidth,
	size_t valIndent,
	size_t valWidth
)
: m_col1(col1)
, m_col2(col2)
, m_keyIndent(keyIndent)
, m_valIndent(valIndent)
, m_keyWidth(keyWidth)
, m_valWidth(valWidth)
{ }

KeyValue::~KeyValue()
{ }

TreeNode *KeyValue::clone() const
{
	return new KeyValue(*this);
}

TreeNode *KeyValue::move()
{
	return new KeyValue(std::move(*this));
}

void KeyValue::dispatch(Page *page) const
{
	page->add(this);
}

void KeyValue::dispatch(Dashh *grammar) const
{
	grammar->add(this);
}

const std::string &KeyValue::col1() const
{
	return m_col1;
}

size_t KeyValue::keyIndent() const
{
	return m_keyIndent;
}

size_t KeyValue::keyWidth() const
{
	return m_keyWidth;
}

const std::string &KeyValue::col2() const
{
	return m_col2;
}

size_t KeyValue::valIndent() const
{
	return m_valIndent;
}

size_t KeyValue::valWidth() const
{
	return m_valWidth;
}

} /* dashh */ 
