#include "Usage.hh"
#include "Utils.hh"
#include "Dashh.hh"
#include "Page.hh"

namespace dashh
{

Usage::Usage(const std::string &usage)
: m_usage(usage)
{ }

Usage::~Usage()
{ }

TreeNode *Usage::clone() const
{
	return new Usage(*this);
}

TreeNode *Usage::move()
{
	return new Usage(std::move(*this));
}

void Usage::dispatch(Page *page) const
{
	page->add(this);
}

void Usage::dispatch(Dashh *) const
{ }

std::string Usage::get() const
{
	return m_usage;
}

} /* dashh */ 
