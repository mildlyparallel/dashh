#include "Parser.hh"

#include "Boolean.hh"
#include "Number.hh"
#include "String.hh"
#include "Lambda.hh"
#include "Vector.hh"

namespace dashh
{

Parser::Parser()
: m_parser(nullptr)
{ }

Parser::~Parser()
{
	if (m_parser)
		delete m_parser;
}

Parser::Parser(bool &output)
: m_parser(new Boolean(output))
{ }

Parser::Parser(int &output)
: m_parser(new Number<int>(output))
{ }

Parser::Parser(unsigned &output)
: m_parser(new Number<unsigned>(output))
{ }

Parser::Parser(long &output)
: m_parser(new Number<long>(output))
{ }

Parser::Parser(unsigned long &output)
: m_parser(new Number<unsigned long>(output))
{ }

Parser::Parser(long long &output)
: m_parser(new Number<long long>(output))
{ }

Parser::Parser(unsigned long long &output)
: m_parser(new Number<unsigned long long>(output))
{ }

Parser::Parser(float &output)
: m_parser(new Number<float>(output))
{ }

Parser::Parser(double &output)
: m_parser(new Number<double>(output))
{ }

Parser::Parser(std::string &output)
: m_parser(new String(output))
{ }

Parser::Parser(std::function <bool (
	const std::string &
)> output)
: m_parser(new Lambda(output))
{ }

Parser::Parser(std::function <bool (
	const std::string &,
	const std::string &
)> output)
: m_parser(new Lambda(output))
{ }

Parser::Parser(std::function <bool (
	const std::string &,
	const std::string &,
	const Argument *
)> output)
: m_parser(new Lambda(output))
{ }

Parser::Parser(std::vector<bool> &output)
: m_parser(new Vector<Boolean>(output))
{ }

Parser::Parser(std::vector<std::string> &output)
: m_parser(new Vector<String>(output))
{ }

Parser::Parser(std::vector<unsigned> &output)
: m_parser(new Vector<Number<unsigned> >(output))
{ }

Parser::Parser(std::vector<int> &output)
: m_parser(new Vector<Number<int> >(output))
{ }

Parser::Parser(std::vector<long> &output)
: m_parser(new Vector<Number<long> >(output))
{ }

Parser::Parser(std::vector<unsigned long> &output)
: m_parser(new Vector<Number<unsigned long> >(output))
{ }

Parser::Parser(std::vector<unsigned long long> &output)
: m_parser(new Vector<Number<unsigned long long> >(output))
{ }

Parser::Parser(std::vector<float> &output)
: m_parser(new Vector<Number<float> >(output))
{ }

Parser::Parser(std::vector<double> &output)
: m_parser(new Vector<Number<double> >(output))
{ }

Parser::Parser(const Parser &other)
: m_parser(nullptr)
{
	if (other.m_parser)
		m_parser = other.m_parser->clone();
}

Parser::Parser(Parser &&other)
: m_parser(other.m_parser)
{
	other.m_parser = nullptr;
}

Parser::Result Parser::parse(
	const std::string &key,
	const std::string &value,
	const Argument *argument
) const {
	return m_parser->parse(key, value, argument);
}

Parser *Parser::clone() const
{
	return m_parser->clone();
}

void Parser::print_error_message(
	const std::string &,
	const std::string &,
	const Argument *
) const { }

} /* dashh */ 
