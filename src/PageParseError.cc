#include <cassert>

#include "Argument.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Command.hh"
#include "PageParseError.hh"

#include "Utils.hh"

namespace dashh
{

PageParseError::PageParseError()
{ }

PageParseError::~PageParseError()
{ }

Page *PageParseError::clone() const
{
	return new PageParseError(*this);
}

void PageParseError::build(
		const Argument *,
		const std::vector<std::string> &trace
) { 
	assert(!trace.empty());
	m_cmd = Utils::trace_to_path(trace);
}

void PageParseError::print(const std::string &key, const std::string &value)
{
	fprintf(stderr, "%s: invalid value \'%s\' for \'%s\' argument!\n",
		m_cmd.c_str(),
		value.c_str(),
		key.c_str()
	);
}

void PageParseError::exit() const
{
	std::exit(EXIT_FAILURE);
}

} /*  dashh */ 
