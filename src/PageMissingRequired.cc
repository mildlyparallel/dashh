#include <cassert>

#include "Argument.hh"
#include "Option.hh"
#include "Command.hh"
#include "Positional.hh"
#include "PageMissingRequired.hh"


#include "Utils.hh"

namespace dashh
{

PageMissingRequired::PageMissingRequired()
{ }

PageMissingRequired::~PageMissingRequired()
{ }

void PageMissingRequired::add(const Option *option)
{
	m_missing = Utils::join(option->names(), "|");
}

void PageMissingRequired::add(const Positional *positional)
{
	m_missing = positional->placeholder();
}

void PageMissingRequired::add(const Command *)
{ }

void PageMissingRequired::add(const Flag *)
{ }

void PageMissingRequired::add(const Text *)
{ }

void PageMissingRequired::add(const Usage *)
{ }

void PageMissingRequired::add(const KeyValue *)
{ }

Page *PageMissingRequired::clone() const
{
	return new PageMissingRequired(*this);
}

void PageMissingRequired::build(
		const Argument *argument,
		const std::vector<std::string> &trace
) { 
	assert(!trace.empty());
	m_cmd = Utils::trace_to_path(trace);

	argument->dispatch(this);
}

void PageMissingRequired::print(const std::string &, const std::string &)
{
	fprintf(stderr, "%s: missing \'%s\' argument!\n", m_cmd.c_str(), m_missing.c_str());
}

void PageMissingRequired::exit() const
{
	std::exit(EXIT_FAILURE);
}

} /*  dashh */ 
