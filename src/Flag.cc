#include "Parser.hh"
#include "Flag.hh"
#include "Utils.hh"
#include "Page.hh"
#include "Dashh.hh"

namespace dashh
{

Flag::Flag(
	const Parser &parser,
	const std::string &names,
	const std::string &description,
	Type type
)
: Argument(parser, names, "", description, type)
{ }

Flag::Flag(
	const Page &page,
	const std::string &names,
	const std::string &description,
	Type type
)
: Argument(page, names, "", description, type)
{ }

Flag::~Flag()
{ }

TreeNode *Flag::clone() const
{
	return new Flag(*this);
}

TreeNode *Flag::move()
{
	return new Flag(std::move(*this));
}

void Flag::dispatch(Page *page) const
{
	page->add(this);
}

void Flag::dispatch(Dashh *grammar) const
{
	grammar->add(this);
}

} /* dashh */ 
