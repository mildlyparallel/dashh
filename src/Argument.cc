#include "Argument.hh"
#include "Page.hh"
#include "Utils.hh"

#include <cassert>

namespace dashh
{

Argument::Argument()
: m_parser(nullptr)
, m_page(nullptr)
, m_type(Type::None)
{ }

Argument::Argument(const Argument &other)
: TreeNode(other)
, m_parser(nullptr)
, m_page(nullptr)
, m_names(other.m_names)
, m_placeholder(other.m_placeholder)
, m_defaults(other.m_defaults)
, m_description(other.m_description)
, m_type(other.m_type)
{
	if (other.m_parser)
		m_parser = other.m_parser->clone();
	if (other.m_page)
		m_page = other.m_page->clone();
}

Argument::Argument(Argument &&other) noexcept 
: TreeNode(std::move(other))
, m_parser(other.m_parser)
, m_page(other.m_page)
, m_names(std::move(other.m_names))
, m_placeholder(std::move(other.m_placeholder))
, m_defaults(std::move(other.m_defaults))
, m_description(std::move(other.m_description))
, m_type(other.m_type) 
{
	other.m_parser = nullptr;
	other.m_page = nullptr;
}

Argument::Argument(
	const Parser &parser,
	const std::string &names,
	const std::string &placeholder,
	const std::string &description,
	Type type
)
: m_parser(parser.clone())
, m_page(nullptr)
, m_names(split(names, "|,"))
, m_placeholder(placeholder)
, m_description(description)
, m_type(type)
{ }

Argument::Argument(
	const Page &page,
	const std::string &names,
	const std::string &placeholder,
	const std::string &description,
	Type type
)
: m_parser(nullptr)
, m_page(page.clone())
, m_names(split(names, "|,"))
, m_placeholder(placeholder)
, m_description(description)
, m_type(type)
{ }

Argument::~Argument()
{
	if (m_parser)
		delete m_parser;
	if (m_page)
		delete m_page;
}

std::vector<std::string> Argument::split(
	const std::string &s,
	const std::string &delims
) {
	std::vector<std::string> output;
	std::string item;

	for (auto &c : s) {
		if (delims.find(c) == std::string::npos && !std::isspace(c)) {
			item += c;
		} else if (!item.empty()) {
			output.push_back(item);
			item.clear();
		}
	}

	if (!item.empty())
		output.push_back(item);

	return output;
}

const std::vector<std::string> &Argument::names() const
{
	return m_names;
}

const std::string &Argument::placeholder() const
{
	return m_placeholder;
}

const std::string &Argument::description() const
{
	return m_description;
}

const Type &Argument::type() const
{
	return m_type;
}

Parser::Result Argument::parse(
	const std::string &key,
	const std::string &value,
	const std::vector<std::string> &trace
) const
{
	if (m_parser) {
		auto res = m_parser->parse(key, value, this);
		if (res == Parser::Result::Error)
			m_parser->print_error_message(key, value, this);
		return res;
	}

	if (m_page) {
		m_page->build(this, trace);
		m_page->print(key, value);
		m_page->exit();
	}

	return Parser::Result::Parsed;
}

} /* dashh */ 
