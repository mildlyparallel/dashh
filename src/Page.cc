#include "Page.hh"
#include "Utils.hh"

#include "Flag.hh"
#include "Option.hh"
#include "Positional.hh"
#include "Command.hh"

namespace dashh
{

Page::Page()
{ }

Page::~Page()
{ }

void Page::add(const Command *)
{ }

void Page::add(const Flag *)
{ }

void Page::add(const Positional *)
{ }

void Page::add(const Option *)
{ }

void Page::add(const Text *)
{ }

void Page::add(const KeyValue *)
{ }

void Page::add(const Usage *)
{ }

void Page::build(const Argument *, const std::vector<std::string> &)
{ }

void Page::print(
	const std::string &,
	const std::string &
)
{ }

void Page::exit() const
{ }

std::string Page::wrap(const std::string &text, size_t width)
{
	size_t line = 0;

	size_t p = 0;
	size_t q = text.find(' ');
	if (q == std::string::npos)
		return text;

	std::string ret = text;

	while (q != text.length()) {
		if (ret[q] == ' ') {
			if (p != std::string::npos) {
				size_t w = q - p;
				if (line + w >= width) {
					ret[p] = '\n';
					line = 0;
				}

				line += w;
			}
			p = q;
		}

		if (ret[q] == '\n') {
			line = 0;
			p = std::string::npos;
		}

		q++;
	}

	if (p != std::string::npos) {
		if (line + (q - p) >= width)
			ret[p] = '\n';
	}

	return ret;
}

std::string Page::fit(
	const std::string &col1,
	const std::string &col2,
	size_t keyIndent,
	size_t keyWidth,
	size_t valIndent,
	size_t valWidth
) const
{
	std::string out = std::string(keyIndent, ' ') + col1;

	size_t offset;
	if (col1.length() > keyWidth) {
		offset = keyIndent + keyWidth + valIndent;
		out += "\n";
	} else {
		offset = (keyWidth - col1.length()) + valIndent;
	}

	std::string text = wrap(col2, valWidth);
	Utils::for_line(text, [&](const std::string &line) {
		out += std::string(offset, ' ') + line + "\n";
		offset = keyIndent + keyWidth + valIndent;
	});

	return out;
}

std::string Page::pattern(const Command *command) const
{
	return Utils::join(command->names(), ", ");
}

std::string Page::pattern(const Flag *flag) const
{
	return Utils::join(flag->names(), ", ");
}

std::string Page::pattern(const Positional *positional) const
{
	auto ph = positional->placeholder();

	if (positional->type().multiple()) {
		if (positional->type().required())
			return ph + " [" + ph + "...]";

		return "[" + ph + "...]";
	}

	if (!positional->type().required())
		return "[" + ph + "]";

	return ph;
}

std::string Page::pattern(const Option *option) const
{
	if (option->placeholder().empty())
		return Utils::join(option->names(), ", ") + " VALUE";

	return Utils::join(option->names(), ", ") + " " + option->placeholder();
}

} /* dashh */ 
