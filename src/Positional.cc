#include "Parser.hh"
#include "Positional.hh"
#include "Utils.hh"
#include "Page.hh"
#include "Dashh.hh"

namespace dashh
{

Positional::Positional(
	const Parser &parser,
	const std::string &placeholder,
	const std::string &description,
	Type type
)
: Argument(parser, {}, placeholder, description, type)
{ }

Positional::Positional(
	const Page &page,
	const std::string &placeholder,
	const std::string &description,
	Type type
)
: Argument(page, {}, placeholder, description, type)
{ }

Positional::~Positional()
{ }

TreeNode *Positional::clone() const
{
	return new Positional(*this);
}

TreeNode *Positional::move()
{
	return new Positional(std::move(*this));
}

void Positional::dispatch(Page *page) const
{
	page->add(this);
}

void Positional::dispatch(Dashh *grammar) const
{
	grammar->add(this);
}

} /*  dashh */ 

