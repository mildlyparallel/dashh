#include "Lambda.hh"

#include "Argument.hh"

namespace dashh
{

Lambda::Lambda(std::function <bool (
	const std::string &
)> parser)
: m_output_v(parser)
, m_output_kv(nullptr)
, m_output_kva(nullptr)
{ }

Lambda::Lambda(std::function <bool (
	const std::string &,
	const std::string &
)> parser)
: m_output_v(nullptr)
, m_output_kv(parser)
, m_output_kva(nullptr)
{ }

Lambda::Lambda(std::function <bool (
	const std::string &,
	const std::string &,
	const Argument *
)> parser)
: m_output_v(nullptr)
, m_output_kv(nullptr)
, m_output_kva(parser)
{ }

Lambda::~Lambda()
{ }

Parser::Result Lambda::parse(
	const std::string &key,
	const std::string &value,
	const Argument *argument
) const
{
	bool parsed = false;

	if (m_output_v)
		parsed = m_output_v(value);
	if (m_output_kv)
		parsed = m_output_kv(key, value);
	if (m_output_kva)
		parsed = m_output_kva(key, value, argument);

	return parsed ? Parser::Parsed : Parser::Error;
}

Parser *Lambda::clone() const
{
	return new Lambda(*this);
}

} /* dashh */ 
