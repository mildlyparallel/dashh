#include "gtest/gtest.h"

#include "mocks/ParserMock.hh"
#include "mocks/ArgumentMock.hh"
#include "mocks/PageMock.hh"

#include "Command.hh"
#include "Flag.hh"
#include "Dashh.hh"
#include "Option.hh"
#include "Positional.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

class DashhTest: public Test {
public:
	DashhTest() {};
	virtual ~DashhTest() {};

	void SetUp() {
		ParserMock::calls.clear();
	}

	size_t parse(Dashh &g, const vector<string> &args) {
		auto n = args.size() +  1;
		auto argv = new const char *[n];

		argv[0] = "argv_0";
		for (size_t i = 1; i < n; ++i)
			argv[i] = args[i - 1].c_str();

		size_t rc = g.parse(n, argv);

		delete []argv;

		return rc;
	}

	bool is_called(const string &k, const string &v) {
		for (auto &p : ParserMock::calls) {
			if (p.key == k && p.val == v)
				return true;
		}

		return false;
	}
};

#define EXPECT_BUILD(mock, vect)\
	do { \
		vector<string> v = {"argv_0", vect};\
		EXPECT_CALL(mock, build(_, v));\
	} while(false);

#define EXPECT_BUILD_EMPTY(mock)\
	do { \
		vector<string> v = {"argv_0"};\
		EXPECT_CALL(mock, build(_, v));\
	} while(false);

TEST_F(DashhTest, command__single_command__single_name) {
	Command root;
	root << Command(ParserMock(Parser::Parsed), "A");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"A"}), 2u);

	EXPECT_TRUE(is_called("A", "A"));
}

TEST_F(DashhTest, command__multiple_commands__single_name) {
	Command root;
	root << Command(ParserMock(Parser::Parsed), "A");
	root << Command(ParserMock(Parser::Parsed), "B");
	root << Command(ParserMock(Parser::Parsed), "C");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"B"}), 2u);

	EXPECT_TRUE(is_called("B", "B"));
}

TEST_F(DashhTest, command__single_command__multiple_names) {
	Command root;
	root << Command(ParserMock(Parser::Parsed), "A,B,C");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"B"}), 2u);

	EXPECT_TRUE(is_called("A", "B"));
}

TEST_F(DashhTest, command__multiple_commands__multiple_names) {
	Command root;
	root << Command(ParserMock(Parser::Parsed), "A,B,C");
	root << Command(ParserMock(Parser::Parsed), "X,Y,Z");
	root << Command(ParserMock(Parser::Parsed), "1,2,3");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"2"}), 2u);

	EXPECT_TRUE(is_called("1", "2"));
}

TEST_F(DashhTest, command__nested_commands) {
	Command root;
	Command a(ParserMock(Parser::Parsed), "A,B,C");
	Command b(ParserMock(Parser::Parsed), "X,Y,Z");
	Command c(ParserMock(Parser::Parsed), "1,2,3");

	b << c;
	a << b;
	root << a;
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"C", "Y", "1"}), 4u);

	EXPECT_TRUE(is_called("A", "C"));
	EXPECT_TRUE(is_called("X", "Y"));
	EXPECT_TRUE(is_called("1", "1"));
}

TEST_F(DashhTest, command__greedy_without_tail) {
	Command root;
	Command a(ParserMock(Parser::Parsed), "A,B,C");
	Command b(ParserMock(Parser::Parsed), "X,Y,Z");
	Command c(ParserMock(Parser::Parsed), "1,2,3", "", Type::Greedy);

	b << c;
	a << b;
	root << a;
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"C", "Y", "1"}), 4u);

	EXPECT_TRUE(is_called("A", "C"));
	EXPECT_TRUE(is_called("X", "Y"));
	EXPECT_TRUE(is_called("1", "1"));
}

TEST_F(DashhTest, command__greedy_with_tail) {
	Command root;
	root << Command(ParserMock(Parser::Parsed), "A,C", "", Type::Greedy);
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"C", "Y", "1"}), 4u);

	EXPECT_TRUE(is_called("A", "C"));
	EXPECT_TRUE(is_called("A", "Y"));
	EXPECT_TRUE(is_called("A", "1"));
}

TEST_F(DashhTest, command__unknown) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, print("N", ""));
	EXPECT_CALL(s, exit());

	Command root;
	root << Command(ParserMock(Parser::Parsed), "A");
	Dashh g(&root);
	g.setPageUnknownArg(&s);

	EXPECT_EQ(parse(g, {"N"}), 1u);

	EXPECT_TRUE(ParserMock::calls.empty());
}

TEST_F(DashhTest, command__parse_error) {
	PageMock s;

	EXPECT_BUILD(s, {"A"});
	EXPECT_CALL(s, print("A", "B"));
	EXPECT_CALL(s, exit());

	Command root;
	root << Command(ParserMock(Parser::Error), "A,B");
	Dashh g(&root);
	g.setPageParseError(&s);

	EXPECT_EQ(parse(g, {"B"}), 1u);

	EXPECT_TRUE(is_called("A", "B"));
}

TEST_F(DashhTest, flag__single_flag_single_name) {
	Command root;
	root << Flag(ParserMock(), "-a");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-a"}), 2u);
	EXPECT_TRUE(is_called("-a", "true"));
}

TEST_F(DashhTest, flag__single_flag_multiple_names) {
	Command root;
	root << Flag(ParserMock(), "-a,-b");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-b"}), 2u);
	EXPECT_TRUE(is_called("-b", "true"));
}

TEST_F(DashhTest, flag__multiple_flags_single_name) {
	Command root;
	root << Flag(ParserMock(), "-a");
	root << Flag(ParserMock(), "-b");
	root << Flag(ParserMock(), "-c");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-b"}), 2u);
	EXPECT_TRUE(is_called("-b", "true"));
}

TEST_F(DashhTest, flag__multiple_flags_multiple_names) {
	Command root;
	root << Flag(ParserMock(), "-a,-b,-c");
	root << Flag(ParserMock(), "-1,-2,-3");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-3"}), 2u);
	EXPECT_TRUE(is_called("-3", "true"));
}

TEST_F(DashhTest, flag__multiple_flags_multiple_args) {
	Command root;
	root << Flag(ParserMock(), "-a,-b,-c");
	root << Flag(ParserMock(), "-1,-2,-3");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-3", "-b", "-1"}), 4u);
	EXPECT_TRUE(is_called("-3", "true"));
	EXPECT_TRUE(is_called("-b", "true"));
	EXPECT_TRUE(is_called("-1", "true"));
}

TEST_F(DashhTest, flag__joined_names) {
	Command root;
	root << Flag(ParserMock(), "-a,-b,-c");
	root << Flag(ParserMock(), "-1,-2,-3");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-3b1"}), 2u);
	EXPECT_TRUE(is_called("-3", "true"));
	EXPECT_TRUE(is_called("-b", "true"));
	EXPECT_TRUE(is_called("-1", "true"));
}

TEST_F(DashhTest, flag__long_and_short_names) {
	Command root;
	root << Flag(ParserMock(), "-a,--bb");
	root << Flag(ParserMock(), "-1,--22");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-a1", "--bb"}), 3u);
	EXPECT_TRUE(is_called("-a", "true"));
	EXPECT_TRUE(is_called("-a", "true"));
	EXPECT_TRUE(is_called("--bb", "true"));
}

TEST_F(DashhTest, flag__parse_error) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, print("-a", "true"));
	EXPECT_CALL(s, exit());

	Command root;
	Flag f(ParserMock(Parser::Error), "-a");

	root << f;
	Dashh g(&root);
	g.setPageParseError(&s);

	EXPECT_EQ(parse(g, {"-a"}), 1u);
	EXPECT_TRUE(is_called("-a", "true"));
}

TEST_F(DashhTest, option__single_short__empty) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, print("-a", ""));
	EXPECT_CALL(s, exit());

	Command root;
	Option o(ParserMock(), "-a");

	root << o;
	Dashh g(&root);
	g.setPageMissingValue(&s);

	EXPECT_EQ(parse(g, {"-a"}), 1u);
}

TEST_F(DashhTest, option__single_short__seprate) {
	Command root;
	root << Option(ParserMock(), "-a");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-a", "x"}), 3u);
	EXPECT_TRUE(is_called("-a", "x"));
}

TEST_F(DashhTest, option__single_short__joined) {
	Command root;
	root << Option(ParserMock(), "-a");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-ax"}), 2u);
	EXPECT_TRUE(is_called("-a", "x"));
}

TEST_F(DashhTest, option__single_short__multiple_values) {
	Command root;
	root << Option(ParserMock(), "-a");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-a1", "-a", "2"}), 4u);
	EXPECT_TRUE(is_called("-a", "1"));
	EXPECT_TRUE(is_called("-a", "2"));
}

TEST_F(DashhTest, option__single_short__multiple_names) {
	Command root;
	root << Option(ParserMock(), "-a,-b");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"-a1", "-b", "2"}), 4u);
	EXPECT_TRUE(is_called("-a", "1"));
	EXPECT_TRUE(is_called("-b", "2"));
}

TEST_F(DashhTest, option__single_long__empty) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, print("--a", ""));
	EXPECT_CALL(s, exit());

	Command root;
	root << Option(ParserMock(), "--a");
	Dashh g(&root);
	g.setPageMissingValue(&s);

	EXPECT_EQ(parse(g, {"--a"}), 1u);
}

TEST_F(DashhTest, option__single_long__seprate) {
	Command root;
	root << Option(ParserMock(), "--aa");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"--aa", "x"}), 3u);
	EXPECT_TRUE(is_called("--aa", "x"));
}

TEST_F(DashhTest, option__single_long__joined) {
	Command root;
	root << Option(ParserMock(), "--a");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"--a=x"}), 2u);
	EXPECT_TRUE(is_called("--a", "x"));
}

TEST_F(DashhTest, option__single_long__multiple_values) {
	Command root;
	root << Option(ParserMock(), "--aa");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"--aa=1", "--aa", "2"}), 4u);
	EXPECT_TRUE(is_called("--aa", "1"));
	EXPECT_TRUE(is_called("--aa", "2"));
}

TEST_F(DashhTest, option__single_long__multiple_names) {
	Command root;
	root << Option(ParserMock(), "--aa,--bb");
	Dashh g(&root);

	EXPECT_EQ(parse(g, {"--aa=1", "--bb", "2"}), 4u);
	EXPECT_TRUE(is_called("--aa", "1"));
	EXPECT_TRUE(is_called("--bb", "2"));
}

TEST_F(DashhTest, option__short_joined__parse_error) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, print("-ax", "x"));
	EXPECT_CALL(s, exit());

	Command root;
	root << Option(ParserMock(Parser::Error), "-a");
	Dashh g(&root);
	g.setPageParseError(&s);

	EXPECT_EQ(parse(g, {"-ax"}), 1u);
	EXPECT_TRUE(is_called("-a", "x"));
}

TEST_F(DashhTest, option__short_separate__parse_error) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, print("-a", "x"));
	EXPECT_CALL(s, exit());

	Command root;
	root << Option(ParserMock(Parser::Error), "-a");

	Dashh g(&root);
	g.setPageParseError(&s);

	EXPECT_EQ(parse(g, {"-a", "x"}), 1u);
	EXPECT_TRUE(is_called("-a", "x"));
}

TEST_F(DashhTest, option__long_joined__parse_error) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, print("--aa=x", "x"));
	EXPECT_CALL(s, exit());

	Command root;
	root << Option(ParserMock(Parser::Error), "--aa");

	Dashh g(&root);
	g.setPageParseError(&s);
	EXPECT_EQ(parse(g, {"--aa=x"}), 1u);

	EXPECT_TRUE(is_called("--aa", "x"));
}

TEST_F(DashhTest, option__long_separate__parse_error) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, exit());
	EXPECT_CALL(s, print("--aa", "x"));

	Command root;
	root << Option(ParserMock(Parser::Error), "--aa");

	Dashh g(&root);
	g.setPageParseError(&s);

	EXPECT_EQ(parse(g, {"--aa", "x"}), 1u);

	EXPECT_TRUE(is_called("--aa", "x"));
}

TEST_F(DashhTest, option_required_not_set__prints_page) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, exit());
	EXPECT_CALL(s, print("", ""));

	Command root;
	root << Option(ParserMock(), "--aa");
	root << Option(ParserMock(), "--bb", "", "", Type::Required);

	Dashh g(&root);
	g.setPageMissingRequired(&s);

	EXPECT_EQ(parse(g, {"--aa", "x"}), 3u);

	EXPECT_TRUE(is_called("--aa", "x"));
}

TEST_F(DashhTest, positional__single) {
	Command root;
	root << Positional(ParserMock(), "a");

	Dashh g(&root);
	EXPECT_EQ(parse(g, {"aaa"}), 2u);

	EXPECT_TRUE(is_called("a", "aaa"));
}

TEST_F(DashhTest, positional__two_single) {
	Command root;
	root << Positional(ParserMock(), "a");
	root << Positional(ParserMock(), "b");

	Dashh g(&root);
	EXPECT_EQ(parse(g, {"aaa", "bbb"}), 3u);

	EXPECT_TRUE(is_called("a", "aaa"));
	EXPECT_TRUE(is_called("b", "bbb"));
}

TEST_F(DashhTest, positional__multiple) {
	Command root;
	root << Positional(ParserMock(), "a", "", Type::Multiple);

	Dashh g(&root);
	EXPECT_EQ(parse(g, {"aaa", "bbb"}), 3u);

	EXPECT_TRUE(is_called("a", "aaa"));
	EXPECT_TRUE(is_called("a", "bbb"));
}

TEST_F(DashhTest, positional__two_single_with_multiple) {
	Command root;
	root << Positional(ParserMock(), "a", "", Type::Multiple);
	root << Positional(ParserMock(), "b");

	Dashh g(&root);
	EXPECT_EQ(parse(g, {"aaa", "bbb"}), 3u);

	EXPECT_TRUE(is_called("a", "aaa"));
	EXPECT_TRUE(is_called("a", "bbb"));
	EXPECT_FALSE(is_called("b", "bbb"));
}

TEST_F(DashhTest, positional__parse_error) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, print("a", "aaa"));
	EXPECT_CALL(s, exit());

	Command root;
	root << Positional(ParserMock(Parser::Error), "a");

	Dashh g(&root);
	g.setPageParseError(&s);

	EXPECT_EQ(parse(g, {"aaa"}), 1u);
	EXPECT_TRUE(is_called("a", "aaa"));
}

TEST_F(DashhTest, positional__required_not_set__prints_page) {
	PageMock s;

	EXPECT_BUILD_EMPTY(s);
	EXPECT_CALL(s, exit());
	EXPECT_CALL(s, print("", ""));

	Command root;
	root << Positional(ParserMock(), "aa");
	root << Positional(ParserMock(), "bb", "", Type::Required);

	Dashh g(&root);
	g.setPageMissingRequired(&s);

	EXPECT_EQ(parse(g, {"11"}), 2u);

	EXPECT_TRUE(is_called("aa", "11"));
}

TEST_F(DashhTest, command__redefines_command) {
	Command root;
	root << Command(ParserMock(), "A");
	root << Command(ParserMock(), "A");
	
	EXPECT_THROW({ Dashh f(&root); }, invalid_argument);
}

TEST_F(DashhTest, command__redefines_flag) {
	Command root;
	root << Command(ParserMock(), "A");
	root << Flag(ParserMock(), "A");
	
	EXPECT_THROW({ Dashh f(&root); }, invalid_argument);
}

TEST_F(DashhTest, command__redefines_option) {
	Command root;
	root << Command(ParserMock(), "A");
	root << Option(ParserMock(), "A");
	
	EXPECT_THROW({ Dashh f(&root); }, invalid_argument);
}

TEST_F(DashhTest, flag__redefines_command) {
	Command root;
	root << Flag(ParserMock(), "A");
	root << Command(ParserMock(), "A");
	
	EXPECT_THROW({ Dashh f(&root); }, invalid_argument);
}

TEST_F(DashhTest, flag__redefines_option) {
	Command root;
	root << Flag(ParserMock(), "A");
	root << Command(ParserMock(), "A");
	
	EXPECT_THROW({ Dashh f(&root); }, invalid_argument);
}

TEST_F(DashhTest, option__redefines_command) {
	Command root;
	root << Option(ParserMock(), "A");
	root << Command(ParserMock(), "A");
	
	EXPECT_THROW({ Dashh f(&root); }, invalid_argument);
}
