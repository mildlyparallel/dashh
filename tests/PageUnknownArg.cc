#include "gtest/gtest.h"

#include "Command.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Dashh.hh"
#include "PageUnknownArg.hh"
#include "mocks/ParserMock.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(pageunknownarg, call_from_option) {
	Option opt(ParserMock(), "-a");

	PageUnknownArg sv;
	sv.build(&opt, {"argv_0", "cmd"});

	testing::internal::CaptureStderr();

	sv.print("-a");

	string output = testing::internal::GetCapturedStderr();

	std::cout << output << std::endl;
	std::string expected =
R"(argv_0 cmd: unknown argument: '-a'

Did you mean this:
  -a VALUE                      
)";

	EXPECT_EQ(output, expected);

	EXPECT_EXIT({sv.exit();}, ExitedWithCode(1), ".*");
}

