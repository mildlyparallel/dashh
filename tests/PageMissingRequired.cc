#include "gtest/gtest.h"

#include "Command.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Positional.hh"
#include "Dashh.hh"
#include "PageMissingRequired.hh"
#include "mocks/ParserMock.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(pagemissingrequired, call_from_option) {
	Option opt(ParserMock(), "-a");

	PageMissingRequired sv;
	sv.build(&opt, {"argv_0", "cmd"});

	testing::internal::CaptureStderr();

	sv.print("-a");

	string output = testing::internal::GetCapturedStderr();

	EXPECT_EQ(output, "argv_0 cmd: missing \'-a\' argument!\n");

	EXPECT_EXIT({sv.exit();}, ExitedWithCode(1), ".*");
}

TEST(pagemissingrequired, call_from_positional) {
	Positional positional(ParserMock(), "FILE");

	PageMissingRequired sv;
	sv.build(&positional, {"argv_0", "cmd"});

	testing::internal::CaptureStderr();

	sv.print("-a");

	string output = testing::internal::GetCapturedStderr();

	EXPECT_EQ(output, "argv_0 cmd: missing \'FILE\' argument!\n");

	EXPECT_EXIT({sv.exit();}, ExitedWithCode(1), ".*");
}

