#include "gtest/gtest.h"

#include "Context.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(ctor, noargs__not_last) {
	Context ctx;
	EXPECT_FALSE(ctx.last());
}

TEST(ctor, noargs__at_0) {
	Context ctx;
	EXPECT_EQ(ctx.position(), 0);
}

TEST(ctor, noargs__at_end) {
	Context ctx;
	EXPECT_TRUE(ctx.end());
}

TEST(ctor, empty_list__at_0) {
	Context ctx(0, nullptr);
	EXPECT_EQ(ctx.position(), 0);
}

TEST(ctor, empty_list__at_end) {
	Context ctx(0, nullptr);
	EXPECT_TRUE(ctx.end());
}

TEST(ctor, empty_list__not_last) {
	Context ctx(0, nullptr);
	EXPECT_FALSE(ctx.last());
}

TEST(ctor, with_items__at_0) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	EXPECT_EQ(ctx.position(), 0);
}

TEST(ctor, with_items__not_last) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	EXPECT_FALSE(ctx.last());
}

TEST(ctor, with_items__not_end) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	EXPECT_FALSE(ctx.end());
}

TEST(travel, access_via_current) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	EXPECT_EQ(ctx.current(), "a1");
	ctx.advance();
	EXPECT_EQ(ctx.current(), "a2");
	ctx.advance();
	EXPECT_EQ(ctx.current(), "a3");
}

TEST(travel, access_via_next) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	EXPECT_EQ(ctx.next(), "a2");
	ctx.advance();
	EXPECT_EQ(ctx.next(), "a3");
}

TEST(travel, over_bounds) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	ctx.advance();
	ctx.advance();
	ctx.advance();
	ctx.advance();
	EXPECT_EQ(ctx.position(), 3);
	EXPECT_TRUE(ctx.end());
}

TEST(travel, end) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	EXPECT_FALSE(ctx.end());
	ctx.advance();
	EXPECT_FALSE(ctx.end());
	ctx.advance();
	EXPECT_FALSE(ctx.end());
	ctx.advance();
	EXPECT_TRUE(ctx.end());
}

TEST(travel, last) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	EXPECT_FALSE(ctx.last());
	ctx.advance();
	EXPECT_FALSE(ctx.last());
	ctx.advance();
	EXPECT_TRUE(ctx.last());
	ctx.advance();
	EXPECT_FALSE(ctx.last());
}

TEST(travel, position) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	EXPECT_EQ(ctx.position(), 0);
	ctx.advance();
	EXPECT_EQ(ctx.position(), 1);
	ctx.advance();
	EXPECT_EQ(ctx.position(), 2);
	ctx.advance();
	EXPECT_EQ(ctx.position(), 3);
}

TEST(travel, finilize) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	EXPECT_EQ(ctx.position(), 0);
	ctx.advance();

	ctx.finilize();

	EXPECT_EQ(ctx.position(), 1);
	EXPECT_TRUE(ctx.end());
	EXPECT_FALSE(ctx.last());
}

TEST(travel, finilize_at_last) {
	char const *argv[] = {"a1", "a2", "a3"};
	Context ctx(3, argv);

	ctx.advance();
	ctx.advance();

	EXPECT_FALSE(ctx.end());
	EXPECT_TRUE(ctx.last());

	ctx.finilize();

	EXPECT_TRUE(ctx.end());
	EXPECT_FALSE(ctx.last());
}
