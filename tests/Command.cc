#include "gtest/gtest.h"

#include "mocks/ParserMock.hh"
#include "mocks/ArgumentMock.hh"
#include "mocks/PageMock.hh"

#include "Command.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

ParserMock parser;

TEST(ctor, default__description_is_empty) {
	Command c;
	EXPECT_TRUE(c.description().empty());
}

TEST(ctor, default__version_is_empty) {
	Command c;
	EXPECT_TRUE(c.version().empty());
}

TEST(ctor, default__usage_is_empty) {
	Command c;
	EXPECT_TRUE(c.usage().empty());
}

TEST(ctor, default__names_are_empty) {
	Command c;
	EXPECT_TRUE(c.names().empty());
}

TEST(ctor, default__type_is_nonde) {
	Command c;
	EXPECT_TRUE(c.type().none());
}

TEST(ctor, description_is_set) {
	Command c(parser, "p", "d");
	EXPECT_EQ(c.description(), "d");
}

TEST(ctor, version_is_empty) {
	Command c(parser, "p", "d");
	EXPECT_TRUE(c.version().empty());
}

TEST(ctor, usage_is_empty) {
	Command c(parser, "p", "d");
	EXPECT_TRUE(c.usage().empty());
}

TEST(ctor, single_name__name_is_set) {
	Command c(parser, "p", "d");
	ASSERT_EQ(c.names().size(), 1u);
	EXPECT_EQ(c.names()[0], "p");
}

TEST(ctor, multiple_names__names_are_set) {
	Command c(parser, "p,q", "d");
	ASSERT_EQ(c.names().size(), 2u);
	EXPECT_EQ(c.names()[0], "p");
	EXPECT_EQ(c.names()[1], "q");
}

TEST(ctor, type_is_nonde) {
	Command c(parser, "p", "d");
	EXPECT_TRUE(c.type().none());
}

TEST(ctor, with_type__type_is_nonde) {
	Command c(parser, "p", "d", Type::Hidden);
	EXPECT_EQ(c.type(), Type::Hidden);
}

TEST(settersgetters, description_is_updated) {
	Command c;
	c.setDescription("d2");
	EXPECT_EQ(c.description(), "d2");
}

TEST(settersgetters, version_is_updated) {
	Command c;
	c.setVersion("v2");
	EXPECT_EQ(c.version(), "v2");
}

TEST(operator_insert, add_argumnent) {
	Command c;
	ArgumentMock a("a1");

	c << a;

	ASSERT_NE(c.child(), nullptr);
	ArgumentMock *aa = static_cast<ArgumentMock *> (c.child());
	EXPECT_EQ(aa->name, "a1");
}

TEST(dispatch_page, page_add_called) {
	Command c;
	PageMock s;

	EXPECT_CALL(s, add(&c));

	c.dispatch(&s);
}

