#include "gtest/gtest.h"

#include "Clamped.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

template <typename T>
class ClampedTest : public ::testing::Test {
	public: T output;
};

using MyTypes = ::testing::Types<int, long, unsigned long, long long, unsigned long long, float, double, long double>;

#if defined(TYPED_TEST_SUITE)
TYPED_TEST_SUITE(ClampedTest, MyTypes);
#else
TYPED_TEST_CASE(ClampedTest, MyTypes);
#endif


TYPED_TEST(ClampedTest, parse_valid) {
	Clamped<TypeParam> parser(this->output, 10, 60);
	Parser::Result result = Parser::Error;

	if (std::is_integral<TypeParam>::value) {
		result = parser.parse("key", "42", nullptr);
		EXPECT_EQ(this->output, 42);
	} else {
		result = parser.parse("key", "42.5", nullptr);
		EXPECT_NEAR(this->output, 42.5, 0.01);
	}

	EXPECT_EQ(result, Parser::Parsed);
}

TYPED_TEST(ClampedTest, parse_low) {
	Clamped<TypeParam> parser(this->output, 10, 60);
	Parser::Result result = Parser::Error;

	if (std::is_integral<TypeParam>::value) {
		result = parser.parse("key", "8", nullptr);
		EXPECT_EQ(this->output, 10);
	} else {
		result = parser.parse("key", "8.1", nullptr);
		EXPECT_NEAR(this->output, 10., 0.01);
	}

	EXPECT_EQ(result, Parser::Parsed);
}

TYPED_TEST(ClampedTest, parse_high) {
	Clamped<TypeParam> parser(this->output, 10, 60);
	Parser::Result result = Parser::Error;

	if (std::is_integral<TypeParam>::value) {
		result = parser.parse("key", "90", nullptr);
		EXPECT_EQ(this->output, 60);
	} else {
		result = parser.parse("key", "90.1", nullptr);
		EXPECT_NEAR(this->output, 60., 0.01);
	}

	EXPECT_EQ(result, Parser::Parsed);
}

TYPED_TEST(ClampedTest, parse_hex) {
	if (!std::is_integral<TypeParam>::value) {
		EXPECT_EQ(1, 1);
		return;
	}

	Clamped<TypeParam> parser(this->output, 10, 60, 16);
	Parser::Result result = Parser::Error;

	result = parser.parse("key", "2a", nullptr);
	EXPECT_EQ(this->output, 42);

	EXPECT_EQ(result, Parser::Parsed);
}

TYPED_TEST(ClampedTest, parse_from_copy) {
	Clamped<TypeParam> parser(this->output, 10, 60);
	Parser *p = parser.clone();

	Parser::Result result = Parser::Error;

	if (std::is_integral<TypeParam>::value) {
		result = p->parse("key", "42", nullptr);
		EXPECT_EQ(this->output, 42);
	} else {
		result = p->parse("key", "42.5", nullptr);
		EXPECT_NEAR(this->output, 42.5, 0.01);
	}

	EXPECT_EQ(result, Parser::Parsed);

	delete p;
}

TYPED_TEST(ClampedTest, parse_invalid) {
	Clamped<TypeParam> parser(this->output, 0, 10);

	Parser::Result result = parser.parse("key", "42aa", nullptr);

	EXPECT_EQ(result, Parser::Error);
}

