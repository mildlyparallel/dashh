#include "gtest/gtest.h"

#include "List.hh"
#include "String.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(parse, list_of_strings) {
	std::vector<std::string> output;
	auto parser = as_list(output);

	Parser::Result result = parser.parse("key", "v1,v2,v3", nullptr);

	EXPECT_EQ(result, Parser::Parsed);
	ASSERT_EQ(output.size(), 3);
	ASSERT_EQ(output[0], "v1");
	ASSERT_EQ(output[1], "v2");
	ASSERT_EQ(output[2], "v3");
}

TEST(parse, list_of_bools) {
	std::vector<bool> output;
	auto parser = as_list(output);

	Parser::Result result = parser.parse("key", "1,false,T", nullptr);

	EXPECT_EQ(result, Parser::Parsed);
	ASSERT_EQ(output.size(), 3);
	ASSERT_EQ(output[0], true);
	ASSERT_EQ(output[1], false);
	ASSERT_EQ(output[2], true);
}

TEST(parse, list_of_int) {
	std::vector<int> output;
	auto parser = as_list(output);

	Parser::Result result = parser.parse("key", "1,3,-3", nullptr);

	EXPECT_EQ(result, Parser::Parsed);
	ASSERT_EQ(output.size(), 3);
	ASSERT_EQ(output[0], 1);
	ASSERT_EQ(output[1], 3);
	ASSERT_EQ(output[2], -3);
}

TEST(parse, list_of_int_parse_error) {
	std::vector<int> output;
	auto parser = as_list(output);

	Parser::Result result = parser.parse("key", "1,xx,-3", nullptr);

	EXPECT_EQ(result, Parser::Error);
	ASSERT_EQ(output.size(), 1);
	ASSERT_EQ(output[0], 1);
}

TEST(clone, output_is_stored_thru_clone) {
	std::vector<std::string> output;
	auto parser = as_list(output);

	Parser *p = parser.clone();

	Parser::Result result = p->parse("key", "v1,v2,v3", nullptr);

	EXPECT_EQ(result, Parser::Parsed);
	ASSERT_EQ(output.size(), 3);
	ASSERT_EQ(output[0], "v1");
	ASSERT_EQ(output[1], "v2");
	ASSERT_EQ(output[2], "v3");

	delete p;
}

