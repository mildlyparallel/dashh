#include "gtest/gtest.h"

#include "Boolean.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(parse, parsed_as_true) {
	bool output;
	Boolean parser(output);

	for (auto v : {
		"1",
		"true",
		"True",
		"TRUE",
		"t",
		"T",
		"y",
		"Y",
		"yes",
		"Yes",
		"YES",
		"on",
		"On",
		"ON"
	}) {
		output = false;
		Parser::Result result = parser.parse("key", v, nullptr);
		EXPECT_EQ(output, true);
		EXPECT_EQ(result, Parser::Parsed);
	}
}

TEST(parse, parsed_as_false) {
	bool output;
	Boolean parser(output);

	for (auto v : {
		"0",
		"false",
		"False",
		"FALSE",
		"f",
		"F",
		"n",
		"N",
		"no",
		"No",
		"NO",
		"off",
		"Off",
		"OFF"
	}) {
		output = true;
		Parser::Result result = parser.parse("key", v, nullptr);
		EXPECT_EQ(output, false);
		EXPECT_EQ(result, Parser::Parsed);
	}
}

TEST(parse, parse_error) {
	bool output = false;
	Boolean parser(output);

	for (auto v : {"9", "aa"}) {
		Parser::Result result = parser.parse("key", v, nullptr);
		EXPECT_EQ(result, Parser::Error);
	}
}

TEST(clone, parser_from_copy) {
	bool output = false;
	Boolean parser(output);

	Parser *p = parser.clone();

	Parser::Result result = p->parse("key", "true", nullptr);

	EXPECT_EQ(output, true);
	EXPECT_EQ(result, Parser::Parsed);

	delete p;
}

