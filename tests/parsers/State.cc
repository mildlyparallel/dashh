#include "gtest/gtest.h"

#include "State.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(parse, value_is_found) {
	string output;
	State parser(output, {"a", "B"});

	Parser::Result result = parser.parse("key", "a", nullptr);

	EXPECT_EQ(output, "a");
	EXPECT_EQ(result, Parser::Parsed);
}

TEST(parse, value_is_not_found) {
	string output = "0";
	State parser(output, {"a", "B"});

	Parser::Result result = parser.parse("key", "C", nullptr);

	EXPECT_EQ(output, "0");
	EXPECT_EQ(result, Parser::Error);
}

TEST(parse, value_is_found__state_upper) {
	string output;
	State parser(output, {"a", "B"});

	Parser::Result result = parser.parse("key", "b", nullptr);

	EXPECT_EQ(output, "b");
	EXPECT_EQ(result, Parser::Parsed);
}

TEST(parse, value_is_found__value_upper) {
	string output;
	State parser(output, {"a", "B"});

	Parser::Result result = parser.parse("key", "A", nullptr);

	EXPECT_EQ(output, "A");
	EXPECT_EQ(result, Parser::Parsed);
}

TEST(clone, output_is_stored_thru_clone) {
	string output;
	State parser(output, {"s"});

	Parser *p = parser.clone();

	Parser::Result result = p->parse("key", "s", nullptr);

	EXPECT_EQ(output, "s");
	EXPECT_EQ(result, Parser::Parsed);

	delete p;
}

