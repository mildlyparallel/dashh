#include "gtest/gtest.h"

#include "Number.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

template <typename T>
class NumberTest : public ::testing::Test {
	public:
		T output;
};

using MyTypes = ::testing::Types<int, long, unsigned long, long long, unsigned long long, float, double, long double, unsigned>;

#if defined(TYPED_TEST_SUITE)
TYPED_TEST_SUITE(NumberTest, MyTypes);
#else
TYPED_TEST_CASE(NumberTest, MyTypes);
#endif

TYPED_TEST(NumberTest, parse_valid) {
	Number<TypeParam> parser(this->output);

	Parser::Result result = Parser::Error;

	if (std::is_integral<TypeParam>::value) {
		result = parser.parse("key", "42", nullptr);
		EXPECT_EQ(this->output, 42);
	} else {
		result = parser.parse("key", "4.2", nullptr);
		EXPECT_NEAR(this->output, 4.2, 0.01);
	}

	EXPECT_EQ(result, Parser::Parsed);
}

TYPED_TEST(NumberTest, parse_from_copy) {
	Number<TypeParam> parser(this->output);
	Parser *p = parser.clone();

	Parser::Result result = Parser::Error;

	if (std::is_integral<TypeParam>::value) {
		result = p->parse("key", "42", nullptr);
		EXPECT_EQ(this->output, 42);
	} else {
		result = p->parse("key", "4.2", nullptr);
		EXPECT_NEAR(this->output, 4.2, 0.01);
	}

	EXPECT_EQ(result, Parser::Parsed);

	delete p;
}

TYPED_TEST(NumberTest, parse_invalid) {
	Number<TypeParam> parser(this->output);

	Parser::Result result = parser.parse("key", "42aa", nullptr);

	EXPECT_EQ(result, Parser::Error);
}

