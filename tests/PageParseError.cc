#include "gtest/gtest.h"

#include "Command.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Dashh.hh"
#include "PageParseError.hh"
#include "mocks/ParserMock.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(pagemissingvalue, call_from_option) {
	Option opt(ParserMock(), "-a");

	PageParseError sv;
	sv.build(&opt, {"argv_0", "cmd"});

	testing::internal::CaptureStderr();

	sv.print("-a", "val");

	string output = testing::internal::GetCapturedStderr();

	EXPECT_EQ(output, "argv_0 cmd: invalid value \'val\' for \'-a\' argument!\n");

	EXPECT_EXIT({sv.exit();}, ExitedWithCode(1), ".*");
}

