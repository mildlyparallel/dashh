#include "gtest/gtest.h"

#include "mocks/TreeNodeMock.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(ctor, parent_is_null) {
	TreeNodeMock r;
	EXPECT_EQ(r.parent(), nullptr);
}

TEST(ctor, sibling_is_null) {
	TreeNodeMock r;
	EXPECT_EQ(r.sibling(), nullptr);
}

TEST(ctor, child_is_null) {
	TreeNodeMock r;
	EXPECT_EQ(r.child(), nullptr);
}

TEST(ctor, copy__children_are_copyied) {
	// TODO: split into smaller tests
	TreeNodeMock t1("1");
	TreeNodeMock t11("11");
	TreeNodeMock t12("12");
	TreeNodeMock t121("121");

	t1.add(&t11);
	t1.add(&t12);
	t12.add(&t121);

	TreeNodeMock *t2 = dynamic_cast<TreeNodeMock *>(t1.clone());

	TreeNodeMock *t21 = dynamic_cast<TreeNodeMock *>(t2->child());
	TreeNodeMock *t22 = dynamic_cast<TreeNodeMock *>(t21->sibling());
	TreeNodeMock *t221 = dynamic_cast<TreeNodeMock *>(t22->child());

	EXPECT_EQ(t21->parent(), t2);
	EXPECT_EQ(t22->parent(), t2);
	EXPECT_EQ(t221->parent(), t22);

	EXPECT_EQ(t2->name, "1");
	EXPECT_EQ(t21->name, "11");
	EXPECT_EQ(t22->name, "12");
	EXPECT_EQ(t221->name, "121");

	delete t221;
	delete t22;
	delete t21;
	delete t2;
}

TEST(ctor, move__the_same_position) {
	// TODO: split into smaller tests

	TreeNodeMock t1;
	TreeNodeMock t11;
	TreeNodeMock t12;
	TreeNodeMock t121;
	TreeNodeMock t122;
	TreeNodeMock t13;

	t1.add(&t11);
	t1.add(&t12);
	t12.add(&t121);
	t12.add(&t122);
	t1.add(&t13);

	TreeNodeMock &t22 = *dynamic_cast<TreeNodeMock *>(t12.move());

	EXPECT_EQ(t12.child(), nullptr);
	EXPECT_EQ(t12.sibling(), nullptr);
	EXPECT_EQ(t12.parent(), nullptr);

	EXPECT_EQ(t1.child(), &t11);
	EXPECT_EQ(t11.sibling(), &t22);
	EXPECT_EQ(t22.sibling(), &t13);

	EXPECT_EQ(t22.child(), &t121);
	EXPECT_EQ(t121.sibling(), &t122);

	EXPECT_EQ(t22.parent(), &t1);
	EXPECT_EQ(t121.parent(), &t22);
	EXPECT_EQ(t122.parent(), &t22);

	TreeNodeMock &t21 = *dynamic_cast<TreeNodeMock *>(t11.move());

	EXPECT_EQ(t11.child(), nullptr);
	EXPECT_EQ(t11.sibling(), nullptr);
	EXPECT_EQ(t11.parent(), nullptr);

	EXPECT_EQ(t1.child(), &t21);
	EXPECT_EQ(t21.sibling(), &t22);
	EXPECT_EQ(t21.parent(), &t1);

	delete &t22;
	delete &t21;
}

TEST(add, single_child__parent_is_set) {
	TreeNodeMock r;
	TreeNodeMock c;

	r.add(&c);

	EXPECT_EQ(c.parent(), &r);
}

TEST(add, single_child__child_is_set) {
	TreeNodeMock r;
	TreeNodeMock c;

	r.add(&c);

	EXPECT_EQ(r.child(), &c);
}

TEST(add, single_child__sibling_is_null) {
	TreeNodeMock r;
	TreeNodeMock c;

	r.add(&c);

	EXPECT_EQ(r.sibling(), nullptr);
}

TEST(add, multiple_children__child_points_to_first) {
	TreeNodeMock r;
	TreeNodeMock c1;
	TreeNodeMock c2;
	TreeNodeMock c3;

	r.add(&c1);
	r.add(&c2);
	r.add(&c3);

	EXPECT_EQ(r.child(), &c1);
}

TEST(add, multiple_children__sibling_is_set_in_order) {
	TreeNodeMock r;
	TreeNodeMock c1;
	TreeNodeMock c2;
	TreeNodeMock c3;

	r.add(&c1);
	r.add(&c2);
	r.add(&c3);

	EXPECT_EQ(c1.sibling(), &c2);
	EXPECT_EQ(c2.sibling(), &c3);
	EXPECT_EQ(c3.sibling(), nullptr);
}
