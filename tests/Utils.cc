#include "gtest/gtest.h"

#include "Utils.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(utils, for_line__multiple_lines) {
	string lines = 
		"Lorem ipsum dolor sit amet, consetetur\n"
		"sadipscing elitr, sed diam nonumy\n"
		"eirmodtempor invidunt ut labore et\n"
		"dolore magna aliquyam erat, sed diam\n"
		"voluptua. Atvero eos et accusam et\n"
		"justo duo dolores et ea rebum. Stet\n"
		"clita kasd gubergren,no sea takimata\n"
		"sanctus est Lorem ipsum dolor sit\n"
		"amet.\n";

	string res;

	Utils::for_line(lines, [&](const std::string &l) {
		if (!l.empty())
			res += l + "\n";
	});

	EXPECT_EQ(lines, res);
}

TEST(utils, for_line__single_line) {
	string lines = "Flags:";

	string res;

	Utils::for_line(lines, [&](const std::string &l) {
		res += l;
	});

	EXPECT_EQ(lines, res);
}

TEST(utils, for_line__single_line_with_nl) {
	string lines = "Flags:\n";

	vector<string> res;

	Utils::for_line(lines, [&](const std::string &l) {
		res.push_back(l);
	});

	ASSERT_EQ(res.size(), 2);
	ASSERT_EQ(res[0], string("Flags:"));
	ASSERT_EQ(res[1], "");
}

TEST(utils, for_line__single_line_with_nl_in_beginning) {
	string lines = "\nFlags:";

	vector<string> res;

	Utils::for_line(lines, [&](const std::string &l) {
		res.push_back(l);
	});

	ASSERT_EQ(res.size(), 2);
	ASSERT_EQ(res[0], "");
	ASSERT_EQ(res[1], "Flags:");
}
