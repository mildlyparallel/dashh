#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Page.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

class PageMock : public Page
{
public:
	PageMock()
	{ };

	PageMock(const PageMock &s)
	: Page(s)
	{ };

	virtual ~PageMock() {};

	MOCK_METHOD1(add, void(const Command *));
	MOCK_METHOD1(add, void(const Flag *));
	MOCK_METHOD1(add, void(const Positional *));
	MOCK_METHOD1(add, void(const Option *));
	MOCK_METHOD1(add, void(const Text *));

	MOCK_METHOD2(build, void(const Argument *, const std::vector<std::string> &));
	MOCK_METHOD2(print, void(const std::string &, const std::string &));
	MOCK_CONST_METHOD0(exit, void());

	virtual PageMock *clone() const {
		return new PageMock(*this);
	}
};
