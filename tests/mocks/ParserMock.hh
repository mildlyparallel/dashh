#pragma once

#include <memory>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Parser.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

class ParserMock : public Parser
{
public:
	ParserMock(Parser::Result r = Parser::Parsed)
	: m_r(r)
	{  };

	virtual ~ParserMock() {};

	Parser::Result m_r;

	struct call_t {
		const string key;
		const string val;
	};

	static std::vector<call_t> calls;

	virtual Result parse(
		const std::string &k,
		const std::string &v,
		const Argument *
	) const
	{
		calls.push_back({k, v});
		return m_r;
	}

	virtual Parser *clone() const {
		return new ParserMock(*this);
	}
};

std::vector<ParserMock::call_t> ParserMock::calls;
