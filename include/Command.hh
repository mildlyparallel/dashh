#ifndef COMMAND_HH_39928
#define COMMAND_HH_39928

#include <vector>
#include <string>

#include "Argument.hh"
#include "Type.hh"

namespace dashh
{

class Command : public Argument
{
public:
	Command();

	Command(const Command &command);

	Command(Command &&command) noexcept;

	Command(
		const Parser &parser,
		const std::string &names,
		const std::string &description = "",
		Type type = Type::None
	);

	Command(
		const Page &page,
		const std::string &names,
		const std::string &description = "",
		Type type = Type::None
	);

	virtual ~Command();

	virtual TreeNode *clone() const;

	virtual TreeNode *move();

	virtual void dispatch(Page *page) const;

	virtual void dispatch(Dashh *grammar) const;

	Command &operator<<(const std::string &str);

	Command &operator<<(const TreeNode &argument);

	Command &operator<<(TreeNode &argument);

	void setDescription(const std::string &description);

	void setVersion(const std::string &version);

	const std::vector<std::string> &usage() const;

	const std::string version() const;

private:
	std::string m_version;
	std::vector<std::string> m_usage;
	std::vector<TreeNode *> m_children;
};

} /* dashh */ 

#endif /* end of include guard: COMMAND_HH_39928_HH */
