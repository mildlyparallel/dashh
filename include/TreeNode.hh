#ifndef TREENODE_HH_33001
#define TREENODE_HH_33001

namespace dashh
{

class Page;
class Dashh;

class TreeNode
{
public:
	TreeNode();

	TreeNode(const TreeNode &other);

	TreeNode(TreeNode &&other) noexcept;

	virtual ~TreeNode();

	void add(TreeNode *node);

	TreeNode *sibling() const;

	TreeNode *child() const;

	TreeNode *parent() const;

	virtual TreeNode *clone() const = 0;

	virtual TreeNode *move() = 0;

	virtual void dispatch(Page *page) const = 0;

	virtual void dispatch(Dashh *page) const = 0;

protected:
	TreeNode *m_parent;
	TreeNode *m_sibling;
	TreeNode *m_child;
	TreeNode *m_last;
};

} /* dashh */ 

#endif /* end of include guard: TREENODE_HH_33001_HH */
