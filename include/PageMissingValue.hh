#ifndef PAGEMISSINGVALUE_HH_38134
#define PAGEMISSINGVALUE_HH_38134

#include "Page.hh"

namespace dashh
{

class Command;
class Flag;
class Positional;
class Option;
class Text;

class PageMissingValue : public Page
{
public:
	PageMissingValue();

	virtual ~PageMissingValue();

	virtual Page *clone() const;

	virtual void build(
		const Argument *argument,
		const std::vector<std::string> &trace
	);

	virtual void print(
		const std::string &key = std::string(),
		const std::string &value = std::string()
	);

	virtual void exit() const;

private:
	std::string m_cmd;
};

} /* dashh */ 

#endif /* end of include guard: PAGEMISSINGVALUE_HH_38134_HH */
