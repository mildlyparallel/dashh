#ifndef POSITIONAL_HH_93273
#define POSITIONAL_HH_93273

#include <initializer_list>
#include <vector>
#include <string>

#include "Argument.hh"
#include "Type.hh"

namespace dashh
{

class Parser;

class Positional : public Argument
{
public:
	Positional(const Positional &Positional) = default;

	Positional(Positional &&Positional) = default;

	Positional(
		const Parser &parser,
		const std::string &placeholder,
		const std::string &description = "",
		Type type = Type::None
	);

	Positional(
		const Page &page,
		const std::string &placeholder,
		const std::string &description = "",
		Type type = Type::None
	);

	virtual ~Positional();

	virtual TreeNode *clone() const;

	virtual TreeNode *move();

	virtual void dispatch(Page *page) const;

	virtual void dispatch(Dashh *grammar) const;

private:
};

} /* dashh */ 

#endif /* end of include guard: POSITIONAL_HH_93273_HH */
