#ifndef FLAG_HH_18979
#define FLAG_HH_18979

#include <vector>
#include <string>

#include "Argument.hh"
#include "Type.hh"

namespace dashh
{

class Parser;
class Page;

class Flag : public Argument
{
public:
	Flag(const Flag &flag) = default;

	Flag(Flag &&flag) = default;

	Flag(
		const Parser &parser,
		const std::string &names,
		const std::string &description = "",
		Type type = Type::None
	);

	Flag(
		const Page &page,
		const std::string &names,
		const std::string &description = "",
		Type type = Type::None
	);

	virtual ~Flag();

	virtual TreeNode *clone() const;

	virtual TreeNode *move();

	virtual void dispatch(Page *page) const;

	virtual void dispatch(Dashh *grammar) const;

private:

};

} /* dashh */ 

#endif /* end of include guard: FLAG_HH_18979_HH */
