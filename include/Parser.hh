#ifndef PARSER_HH_55061
#define PARSER_HH_55061

#include <functional>
#include <string>
#include <vector>

namespace dashh
{

class Argument;

class Parser
{
public:
	Parser();

	Parser(Parser &&other);

	Parser(const Parser &other);

	virtual ~Parser();

	Parser(bool &output);

	Parser(int &output);

	Parser(long &output);

	Parser(unsigned &output);

	Parser(unsigned long &output);

	Parser(long long &output);

	Parser(unsigned long long &output);

	Parser(float &output);

	Parser(double &output);

	Parser(std::string &output);

	Parser(std::function <bool (
		const std::string &
	)> parser);

	Parser(std::function <bool (
		const std::string &,
		const std::string &
	)> parser);

	Parser(std::function <bool (
		const std::string &,
		const std::string &,
		const Argument *
	)> parser);

	Parser(std::vector<bool> &output);

	Parser(std::vector<std::string> &output);

	Parser(std::vector<int> &output);

	Parser(std::vector<unsigned> &output);

	Parser(std::vector<long> &output);

	Parser(std::vector<unsigned long> &output);

	Parser(std::vector<unsigned long long> &output);

	Parser(std::vector<float> &output);

	Parser(std::vector<double> &output);

	enum Result {
		Parsed,
		Error,
		Break,
	};

	virtual Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	virtual Parser *clone() const;

	virtual void print_error_message(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

protected:
	Parser *m_parser;
};

} /* dashh */ 

#endif /* end of include guard: PARSER_HH_55061_HH */
