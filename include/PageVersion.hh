#ifndef PAGEVERSION_HH_17252
#define PAGEVERSION_HH_17252

#include "Page.hh"

namespace dashh
{

class Command;
class Flag;
class Positional;
class Option;
class Text;
class Usage;
class KeyValue;

class PageVersion : public Page
{
public:
	PageVersion();

	virtual ~PageVersion();

	virtual void add(const Command *command);
	virtual void add(const Flag *flag);
	virtual void add(const Positional *positional);
	virtual void add(const Option *option);
	virtual void add(const Text *text);
	virtual void add(const Usage *text);
	virtual void add(const KeyValue *columns);

	virtual Page *clone() const;

	virtual void build(
		const Argument *argument,
		const std::vector<std::string> &trace
	);

	virtual void print(
		const std::string &key = std::string(),
		const std::string &value = std::string()
	);

	virtual void exit() const;

private:
	std::string m_version;
	std::string m_cmd;
};
} /* dashh */ 

#endif /* end of include guard: PAGEVERSION_HH_17252_HH */
