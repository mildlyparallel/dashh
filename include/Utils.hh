#ifndef UTILS_HH_31186
#define UTILS_HH_31186

#include <vector>
#include <stdexcept>
#include <string>
#include <algorithm>
#include <functional>

namespace dashh
{
namespace Utils
{

unsigned stou(const std::string &s, size_t *pos = 0, int b = 10);

template <typename T>
bool found(const T &key, const std::vector<T> &haystack)
{
	return std::find(haystack.cbegin(), haystack.cend(), key) != haystack.cend();
}

std::string join(const std::vector<std::string> &v, const std::string &s);

void for_line(const std::string &text, std::function<void (const std::string &line)> callback);

std::vector<std::string> split(
	const std::string &s,
	const std::string &delims
);

std::string trace_to_path(const std::vector<std::string> &trace);

} /* Utils */ 
} /* dashh */ 

#endif /* end of include guard: UTILS_HH_31186_HH */
