#ifndef ARGUMENT_HH_86603
#define ARGUMENT_HH_86603

#include <vector>

#include "TreeNode.hh"
#include "Parser.hh"
#include "Type.hh"

namespace dashh
{

class Page;
class Dashh;

class Argument : public TreeNode
{
public:
	Argument();

	Argument(const Argument &other);

	Argument(Argument &&other) noexcept;

	Argument(
		const Parser &parser,
		const std::string &names,
		const std::string &placeholder,
		const std::string &description,
		Type type
	);

	Argument(
		const Page &page,
		const std::string &names,
		const std::string &placeholder,
		const std::string &description,
		Type type
	);

	virtual ~Argument();

	virtual TreeNode *clone() const = 0;

	virtual TreeNode *move() = 0;

	virtual const std::vector<std::string> &names() const;

	virtual const std::string &placeholder() const;

	virtual const std::string &description() const;

	virtual const Type &type() const;

	virtual Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const std::vector<std::string> &trace
	) const;

protected:
	static std::vector<std::string> split(
		const std::string &s,
		const std::string &delims = ",|"
	);

	Parser *m_parser;
	Page *m_page;
	std::vector<std::string> m_names;
	std::string m_placeholder;
	std::string m_defaults;
	std::string m_description;
	Type m_type;

private:

};

} /* dashh */ 

#endif /* end of include guard: ARGUMENT_HH_86603_HH */
