#ifndef PAGEUNKNOWNARG_HH_92394
#define PAGEUNKNOWNARG_HH_92394

#include <tuple>

#include "Page.hh"

namespace dashh
{

class Command;
class Flag;
class Positional;
class Option;
class Text;
class Usage;
class KeyValue;

class PageUnknownArg : public Page
{
public:
	PageUnknownArg();

	virtual ~PageUnknownArg();

	virtual Page *clone() const;

	virtual void add(const Command *command);
	virtual void add(const Flag *flag);
	virtual void add(const Positional *positional);
	virtual void add(const Option *option);
	virtual void add(const Text *text);
	virtual void add(const Usage *text);
	virtual void add(const KeyValue *columns);

	virtual void build(
		const Argument *argument,
		const std::vector<std::string> &trace
	);

	virtual void print(
		const std::string &key = std::string(),
		const std::string &value = std::string()
	);

	virtual void exit() const;

private:
	static constexpr double m_deletionCost = 1.0;
	static constexpr double m_insertionCost = 1.0;
	static constexpr double m_shiftCost = -0.5;

	static constexpr size_t m_maxDistance = 4;
	static constexpr size_t m_maxSuggestions = 3;

	double typoDistance(const std::string &a, const std::string &b);

	double keyboardDistance(char a, char b);

	std::tuple<size_t, size_t, bool> position(char ch);

	std::vector<std::tuple<const Argument *, std::string> > m_alternatives;

	std::string m_cmd;
};

} /* dashh */ 

#endif /* end of include guard: PAGEUNKNOWNARG_HH_92394_HH */
