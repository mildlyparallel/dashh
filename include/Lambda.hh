#ifndef LAMBDA_HH_2310
#define LAMBDA_HH_2310

#include "Parser.hh"

#include <string>
#include <vector>

namespace dashh
{

class Lambda : public Parser
{
public:
	Lambda(std::function <bool (
		const std::string &
	)> parser);

	Lambda(std::function <bool (
		const std::string &,
		const std::string &
	)> parser);

	Lambda(std::function <bool (
		const std::string &,
		const std::string &,
		const Argument *
	)> parser);

	virtual ~Lambda();

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	Parser *clone() const;

private:
	std::function <bool (
		const std::string &
	)> m_output_v;

	std::function <bool (
		const std::string &,
		const std::string &
	)> m_output_kv;

	std::function <bool (
		const std::string &,
		const std::string &,
		const Argument *
	)> m_output_kva;
};

} /* dashh */ 

#endif /* end of include guard: LAMBDA_HH_2310_HH */
