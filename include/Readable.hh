#ifndef READABLE_HH_80953
#define READABLE_HH_80953

#include "Parser.hh"

#include <string>
#include <vector>

namespace dashh
{

class Readable : public Parser
{
public:
	using value_type = std::string;

	Readable(std::string &output);

	virtual ~Readable();

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	Parser *clone() const;

	virtual void print_error_message(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

private:
	std::string &m_output;
};

Readable as_readable(std::string &output);

} /* dashh */ 

#endif /* end of include guard: READABLE_HH_80953_HH */
