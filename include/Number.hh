#ifndef NUMBER_HH_90728
#define NUMBER_HH_90728

#include "Parser.hh"
#include "Utils.hh"
#include <string>
#include <vector>

namespace dashh
{

template <typename T>
class Number : public Parser
{
public:
	using value_type = T;

	Number(T &output, int base = 10);

	virtual ~Number();

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	Parser *clone() const;

private:
	int m_base;
	T &m_output;
};

template <typename T>
Number<T> as_number(T &output, int base = 10) {
	return Number<T>(output, base);
}

template <typename T>
Number<T>::Number(T &output, int base)
: m_base(base)
, m_output(output)
{
	static_assert(std::is_integral<T>::value || std::is_floating_point<T>::value,
		"Specified type T for dashh::Number is not supported");
}

template <typename T>
Number<T>::~Number()
{  }

template <typename T>
Parser::Result Number<T>::parse(
	const std::string &,
	const std::string &value,
	const Argument *
) const
{
	using namespace::std;

	try {
		size_t pos = 0;

		if (std::is_same<T, int>::value)
			m_output = stoi(value, &pos, m_base);
		else if (std::is_same<T, long>::value)
			m_output = stol(value, &pos, m_base);
		else if (std::is_same<T, unsigned long>::value)
			m_output = stoul(value, &pos, m_base);
		else if (std::is_same<T, long long>::value)
			m_output = stoll(value, &pos, m_base);
		else if (std::is_same<T, unsigned long long>::value)
			m_output = stoull(value, &pos, m_base);
		else if (std::is_same<T, float>::value)
			m_output = stof(value, &pos);
		else if (std::is_same<T, double>::value)
			m_output = stod(value, &pos);
		else if (std::is_same<T, long double>::value)
			m_output = stold(value, &pos);
		else if (std::is_same<T, unsigned>::value)
			m_output = Utils::stou(value, &pos);
		else
			throw std::invalid_argument("Specified type T for dashh::Number is not supported");

		return pos == value.length() ? Parser::Parsed : Parser::Error;
	} catch(std::logic_error const &) {
		return Parser::Error;
	}
}

template <typename T>
Parser *Number<T>::clone() const
{
	return new Number<T>(*this);
}


} /* dashh */ 

#endif /* end of include guard: NUMBER_HH_90728_HH */
