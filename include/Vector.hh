#ifndef VECTOR_HH_78866
#define VECTOR_HH_78866

#include "Parser.hh"

#include <string>
#include <vector>

namespace dashh
{

template <typename T>
class Vector : public Parser
{
public:
	Vector(std::vector<typename T::value_type> &output);

	virtual ~Vector();

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	Parser *clone() const;

private:
	std::vector<typename T::value_type> &m_output;
};

template <typename T>
Vector<T>::Vector(std::vector<typename T::value_type> &output)
: m_output(output)
{ }

template <typename T>
Vector<T>::~Vector()
{ }

template <typename T>
Parser::Result Vector<T>::parse(
	const std::string &key,
	const std::string &value,
	const Argument *a
) const
{
	typename T::value_type output;
	T parser(output);
	Parser::Result r = parser.parse(key, value, a);
	if (r == Parser::Parsed)
		m_output.push_back(output);
	return r;
}

template <typename T>
Parser *Vector<T>::clone() const
{
	return new Vector<T>(*this);
}

} /* dashh */ 


#endif /* end of include guard: VECTOR_HH_78866_HH */
