#ifndef CONTEXT_HH_82653
#define CONTEXT_HH_82653

#include <vector>
#include <string>

namespace dashh
{

class Context
{
public:
	Context();

	Context(int argc, char const *argv[]);

	virtual ~Context();

	const std::string &current() const;

	const std::string &next() const;

	void advance();

	void finilize();

	size_t position() const;

	bool last() const;

	bool end() const;

private:
	size_t m_position;
	bool m_finilized;
	std::vector<std::string> m_data;
};

} /* dashh */ 
#endif /* end of include guard: CONTEXT_HH_82653_HH */
