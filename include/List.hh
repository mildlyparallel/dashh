#ifndef LIST_HH_62609
#define LIST_HH_62609

#include "Parser.hh"
#include "Utils.hh"

#include "String.hh"
#include "Number.hh"
#include "Boolean.hh"

#include <string>
#include <vector>

namespace dashh
{

class List: public Parser
{
public:
	List(Parser &&output, const std::string &delim = ",");

	List(const Parser &output, const std::string &delim = ",");

	virtual ~List();

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	Parser *clone() const;

private:
	std::string m_delim;
};

List as_list(const Parser &parser, const std::string &delim = ",");

List as_list(std::vector<std::string> &output, const std::string &delim = ",");

List as_list(std::vector<bool> &output, const std::string &delim=",");

template <typename T, typename = std::enable_if< std::is_arithmetic<T>::value > >
List as_list(std::vector<T> &output, const std::string &delim=",") {
	return List(Parser(output), delim);
}

} /* dashh */ 

#endif /* end of include guard: LIST_HH_62609_HH */
