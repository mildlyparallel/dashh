#ifndef STATE_HH_1704
#define STATE_HH_1704

#include "Parser.hh"

#include <string>
#include <vector>

namespace dashh
{

class State : public Parser
{
public:
	using value_type = std::string;

	State(std::string &output, const std::vector<std::string> &states);
	virtual ~State();

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	Parser *clone() const;

private:
	std::string &m_output;
	std::vector<std::string> m_states;
};

State as_state(std::string &output, const std::vector<std::string> &states);

} /* dashh */ 

#endif /* end of include guard: STATE_HH_1704_HH */
