#ifndef DASHH_HH_68834
#define DASHH_HH_68834

#include <vector> 
#include <string> 

#include "Command.hh"
#include "Flag.hh"
#include "Positional.hh"
#include "Option.hh"
#include "Text.hh"
#include "KeyValue.hh"
#include "Usage.hh"

#include "Boolean.hh"
#include "Lambda.hh"
#include "Number.hh"
#include "State.hh"
#include "String.hh"
#include "Vector.hh"
#include "Clamped.hh"
#include "List.hh"
#include "Readable.hh"

#include "PageHelp.hh"
#include "PageMissingRequired.hh"
#include "PageMissingValue.hh"
#include "PageParseError.hh"
#include "PageUnknownArg.hh"
#include "PageVersion.hh"

namespace dashh
{

class Context;
class Argument;
class Page;

class Dashh
{
public:
	Dashh(const Command *tree);

	virtual ~Dashh();

	virtual size_t parse(int argc, char const *argv[]);

	virtual void add(const Command *command);
	virtual void add(const Flag *flag);
	virtual void add(const Positional *positional);
	virtual void add(const Option *option);
	virtual void add(const Text *text);
	virtual void add(const KeyValue *text);
	virtual void add(const Usage *text);

	virtual void setPageParseError(Page *page);
	virtual void setPageUnknownArg(Page *page);
	virtual void setPageMissingValue(Page *page);
	virtual void setPageMissingRequired(Page *page);

	virtual void disablePageParseError(bool disable = true);
	virtual void disablePageUnknownArg(bool disable = true);
	virtual void disablePageMissingValue(bool disable = true);
	virtual void disablePageMissingRequired(bool disable = true);

protected:
	Page *m_pageParseError;
	Page *m_pageUnknownArg;
	Page *m_pageMissingValue;
	Page *m_pageMissingRequired;

	bool m_disableParseError;
	bool m_disableUnknownArg;
	bool m_disableMissingValue;
	bool m_disableMissingRequired;

private:
	virtual void showParseError(
		const Argument *argument,
		const std::string &key,
		const std::string &val
	) const;

	virtual void showUnknownArg(
		const Argument *argument,
		const std::string &key
	) const;

	virtual void showMissingValue(
		const Argument *argument,
		const std::string &key
	) const;

	virtual void showMissingRequired(const Argument *argument) const;

	void parse(Context &ctx);

	void parseRoot(Context &ctx);

	void parseArgs(Context &ctx);

	bool parseSingleFlags(Context &ctx);
	bool parseJoinedFlags(Context &ctx);
	bool parseSingleOptions(Context &ctx);
	bool parseJoinedOptions(Context &ctx);
	bool parseOptions(Context &ctx);
	bool parsePositionals(Context &ctx);

	void parseCommands(Context &ctx);

	void checkRedefinitions(const std::vector<std::string> &names) const;

	const Argument *findMissingRequired();

	static bool areJoinedFlags(const std::string &s);
	static bool isSingleOption(const std::string &s);
	static bool isJoinedOption(const std::string &s);

	static std::vector<std::string> splitJoinedFlags(
		const std::string &s
	);

	static std::pair<std::string, std::string> splitSingleOption(
		const std::string &s
	);

	static std::pair<std::string, std::string> splitJoinedOption(
		const std::string &s
	);

	static const std::string DEFAULT_TRUE;
	static const std::string DEFAULT_FALSE;

	const Command *m_root;

	std::vector<const Command *> m_commands;
	std::vector<const Flag *> m_flags;
	std::vector<const Positional *> m_positionals;
	std::vector<const Option *> m_options;
	std::vector<const Argument *> m_parsed;
	std::vector<std::string> m_trace;
};

}

#endif /* end of include guard: DASHH_HH_68834_HH */
