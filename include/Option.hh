#ifndef OPTION_HH_8259
#define OPTION_HH_8259

#include <vector>
#include <string>

#include "Argument.hh"
#include "Type.hh"

namespace dashh
{

class Option : public Argument
{
public:
	Option(const Option &option) = default;

	Option(Option &&option) = default;

	Option(
		const Parser &parser,
		const std::string &names,
		const std::string &placeholder = "",
		const std::string &description = "",
		Type type = Type::None
	);

	Option(
		const Page &page,
		const std::string &names,
		const std::string &placeholder = "",
		const std::string &description = "",
		Type type = Type::None
	);

	virtual ~Option();

	virtual TreeNode *clone() const;

	virtual TreeNode *move();

	virtual void dispatch(Page *page) const;

	virtual void dispatch(Dashh *grammar) const;
private:
};

} /* dashh */ 

#endif /* end of include guard: OPTION_HH_8259_HH */
