#include <iostream>
#include <vector>
#include <ostream>

#include "Dashh.hh"

template<typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &v) {
	for (auto x : v)
		os << x << " ";
	return os;
}

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;

	std::vector<bool> bool_list;
	cmd << Option(
		as_list(bool_list),
		"-b, --bool-list",
		"BOOLEANS",
		"A comma-separated list with boolean values"
	);

	std::vector<std::string> string_list;
	cmd << Option(
		as_list(string_list),
		"-s, --string-list",
		"STRINGS",
		"A comma-separated list with strings"
	);

	std::vector<int> int_list;
	cmd << Option(as_list(int_list),
		"-i, --int-list",
		"INTEGERS",
		"Comma-separated list with integer values"
	);

	std::vector<float> float_list;
	cmd << Option(
		as_list(float_list, ",:;"),
		"-f, --float-list",
		"FLOATS",
		"A list with values separated with commas, colons or semicolons"
	);

	std::vector<std::string> custom_strings;
	auto custom_str_parser = Parser([&custom_strings](const std::string &value) {
		if (value.length() > 10) {
			std::cout << "The value of --custom-list must be less than 10 chars\n";
			return false;
		}

		custom_strings.push_back(value);
		return true;
	});

	cmd << Option(
		as_list(custom_str_parser),
		"-c, --custom-list",
		"STIRNGS",
		"A list with a custom parser for its values"
	);

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << "-b, --bool-list: " << bool_list << "\n";
	std::cout << "-s, --string-list: " << string_list << "\n";
	std::cout << "-i, --int-list: " << int_list << "\n";
	std::cout << "-f, --float-list: " << float_list << "\n";
	std::cout << "-c, --custom-list: " << custom_strings << "\n";

	return 0;
}

