#!/bin/bash

set -e

[ -z "$1" ] && exe="./$(basename ${0%.*})" || exe="$1"

[ ! -e "$exe" ] && {
	echo "\$exe is not executable"
	echo "Usage: $0 <executable>"
	exit 1
}

function cmp() {
	echo "Testing '$exe $1'"
	diff <($exe $1 2>&1) <(echo "$2") || exit 1
	echo "OK"
}

