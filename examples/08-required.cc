#include <iostream>
#include <vector>

#include "Dashh.hh"

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;

	int n = 0;
	cmd << Option(n, "-n",
		"N",
		"Required argument",
		Type::Required
	);

	double v = 0;
	cmd << Positional(v,
		"V",
		"Required arguemtn",
		Type::Required
	);

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << "-n: " << n << "\n";
	std::cout << "v: " << v << "\n";

	return 0;
}

