#include <iostream>
#include <vector>

#include "Dashh.hh"

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;

	int n = 0;
	cmd << Option(n, "-n", "N");

	int i = 0;
	cmd << Positional(i, "INT");

	double d = 0;
	cmd << Positional(d, "DOUBLE");

	std::vector<std::string> vs;
	cmd << Positional(vs, "STRING...", "", Type::Multiple);

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << "n: " << n << "\n";
	std::cout << "i: " << i << "\n";
	std::cout << "d: " << d << "\n";
	std::cout << "vs: ";
	for (auto s : vs)
		std::cout << s << " ";
	std::cout << "\n";

	return 0;
}

