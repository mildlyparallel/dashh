#include <iostream>
#include <vector>

#include "Dashh.hh"

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;
	cmd.setVersion("v2.3.1");

	cmd << Flag(PageVersion(), "-v");

	std::string action;
	Command subcmd1(action, "cmd1", "Command 1");
	subcmd1.setVersion("v3.2.1");
	subcmd1 << Flag(PageVersion(), "-v");

	Command subcmd2(action, "cmd2", "Command 2");
	subcmd2 << Flag(PageVersion(), "-v");

	cmd << subcmd1;
	cmd << subcmd2;

	Dashh t(&cmd);
	t.parse(argc, argv);

	return 0;
}

