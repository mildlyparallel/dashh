#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "" "$(cat << EOF
n: 0
i: 0
d: 0
vs: 
EOF
)"

cmp "1" "$(cat << EOF
n: 0
i: 1
d: 0
vs: 
EOF
)"

cmp "1 2" "$(cat << EOF
n: 0
i: 1
d: 2
vs: 
EOF
)"

cmp "1 2 3" "$(cat << EOF
n: 0
i: 1
d: 2
vs: 3 
EOF
)"

cmp "1 2 3 4" "$(cat << EOF
n: 0
i: 1
d: 2
vs: 3 4 
EOF
)"

cmp "x 1 3 4" "$(cat << EOF
12-positional: invalid value 'x' for 'INT' argument!
EOF
)"

cmp "1 x 3 4" "$(cat << EOF
12-positional: invalid value 'x' for 'DOUBLE' argument!
EOF
)"

cmp "1 2 x 4" "$(cat << EOF
n: 0
i: 1
d: 2
vs: x 4 
EOF
)"

cmp "1 2 -n 9 3 4" "$(cat << EOF
n: 9
i: 1
d: 2
vs: 3 4 
EOF
)"
