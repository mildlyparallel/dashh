#include <iostream>
#include <string>

#include "Dashh.hh"

int main(int argc, const char *argv[])
{
	using namespace dashh;

	Command cmd;

	bool verbose = false;
	cmd << Flag(verbose, "-v");

	std::string action;

	Command cmd_write(action, "write");

	std::string output;
	cmd_write << Option(output, "-o, --output", "FILE");
	cmd << cmd_write;

	Command cmd_read(action, "read");

	std::vector<std::string> input;
	cmd_read << Positional(input, "FILE...", "Description", Type::Multiple);

	cmd << cmd_read;

	// Command with alternative names
	Command cmd_list(action, "ls,list,index");

	cmd << cmd_list;

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << std::boolalpha;
	std::cout << "verbose: " << verbose << "\n";
	std::cout << "action: "  << action  << "\n";
	std::cout << "output: "  << output  << "\n";
	std::cout << "input:";
	for (auto &file : input)
		std::cout << " " << file;
	std::cout << "\n"; 

	return 0;
}

