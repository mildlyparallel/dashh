#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "abc" "$(cat << EOF
Can not parse: abc
EOF
)"

cmp "-abc" "$(cat << EOF
Can not parse: -abc
EOF
)"

cmp "-a" "$(cat << EOF
Can not parse: -a
EOF
)"

cmp "-a1 -b3" "$(cat << EOF
-a: 1
-b: 3
EOF
)"
