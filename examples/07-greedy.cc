#include <iostream>
#include <vector>
#include <ostream>

#include "Dashh.hh"

template<typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &v) {
	for (auto &x : v)
		os << x << " ";
	return os;
}

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;

	std::vector<std::string> values[3];

	cmd << Option(values[0], "-, --",
		"ARGS",
		"List of arguments",
		Type::Greedy
	);

	cmd << Option(values[1], "-a, --args",
		"ARGS",
		"List of arguments",
		Type::Greedy
	);

	cmd << Positional(values[2], "ARGS...",
		"List of arguments",
		Type::Greedy
	);

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << "--: " << values[0] << "\n";
	std::cout << "--args: " << values[1] << "\n";
	std::cout << "ARGS...: " << values[2] << "\n";

	return 0;
}

