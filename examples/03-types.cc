#include <iostream>

#include "Dashh.hh"

class MyParser : public dashh::Parser
{
public:
	MyParser(std::string &output)
	: m_output(output)
	{ }

	virtual ~MyParser()
	{ }

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const dashh::Argument *argument
	) const {
		if (value.size() == 3)
			return Result::Error;
		m_output = value;
		return Result::Parsed;
	}

	Parser *clone() const {
		return new MyParser(*this);
	}

	virtual void print_error_message(
		const std::string &key,
		const std::string &value,
		const dashh::Argument *argument
	) const {
		std::cerr << "Error message from custom parser" << std::endl;
		std::cerr << "   key = " << key << std::endl;
		std::cerr << "   value = " << value << std::endl;
	}

private:
	std::string &m_output;
};

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;

	int i = 0;
	cmd << Option(i, "-i, --int", "INTEGER");

	unsigned u = 0;
	cmd << Option(u, "-u, --unsigned", "UNSIGNED");

	long l = 0;
	cmd << Option(u, "-l, --long", "LONG");

	unsigned long ul = 0;
	cmd << Option(ul, "--unsigned-long", "ULONG");

	long long ll = 0;
	cmd << Option(ll, "--long-long", "LLONG");

	unsigned long long ull = 0;
	cmd << Option(ull, "--unsigned-long-long", "ULLONG");

	float f = 0;
	cmd << Option(f, "-f, --float", "FLOAT");

	double d = 0;
	cmd << Option(d, "-d, --double", "DOUBLE");

	unsigned long hexadecimal = 0;
	cmd << Option(as_number(hexadecimal, 16), "-x, --hex", "HEX");

	int clamped = 0;
	cmd << Option(as_clamped(clamped, -10, 10), "-c, --clamped", "INTEGER");

	std::string state;
	cmd << Option(as_state(state, {"one", "two", "three"}), "--state", "one|two|three");

	std::string file;
	cmd << Option(as_readable(file), "--file", "PATH");

	std::string short_string;
	auto ss_par = Parser([&short_string](const std::string &value) {
		if (value.length() > 10)
			return false;

		short_string = value;
		return true;
	});

	cmd << Option(ss_par, "--short-string", "STRING");

	std::string custom_parser;
	cmd << Option(MyParser(custom_parser), "--custom-parser", "STRING");

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << std::boolalpha;
	std::cout << "integer: "            << i            << "\n";
	std::cout << "unsigned: "           << u            << "\n";
	std::cout << "long: "               << l            << "\n";
	std::cout << "unsigned long: "      << ul           << "\n";
	std::cout << "long long: "          << ll           << "\n";
	std::cout << "unsigned long long: " << ull          << "\n";
	std::cout << "float: "              << f            << "\n";
	std::cout << "double: "             << d            << "\n";
	std::cout << "hexadecimal: "        << hexadecimal  << "\n";
	std::cout << "clamped: "            << clamped      << "\n";
	std::cout << "state: "              << state        << "\n";
	std::cout << "file: "               << file         << "\n";
	std::cout << "short_stirng: "       << short_string << "\n";

	return 0;
}
