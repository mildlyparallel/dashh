#include <iostream>
#include <vector>

#include "Dashh.hh"

template<typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &v) {
	for (auto x : v)
		os << x << " ";
	return os;
}

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;

	std::vector<bool> bool_list;
	cmd << Option(
		bool_list,
		"-b, --bool-list",
		"1|0",
		"Multiple values can be specified by repeating this argument"
	);

	std::vector<std::string> string_list;
	cmd << Option(
		string_list,
		"-s, --string-list",
		"STRING",
		"Multiple values can be specified by repeating this argument"
	);

	std::vector<int> int_list;
	cmd << Option(int_list,
		"-i, --int-list",
		"INTEGER",
		"Multiple values can be specified by repeating this argument"
	);

	std::vector<float> float_list;
	cmd << Option(
		float_list,
		"-f, --float-list",
		"FLOAT",
		"Multiple values can be specified by repeating this argument"
	);

	std::vector<std::string> custom_strings;
	auto custom_str_parser = Parser([&custom_strings](const std::string &value) {
		if (value.length() > 10)
			return false;

		custom_strings.push_back(value);
		return true;
	});

	cmd << Option(
		custom_str_parser,
		"-c, --custom-list",
		"STIRNG",
		"Multiple values can be specified by repeating this argument"
	);

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << "-b, --bool-list: " << bool_list << "\n";
	std::cout << "-s, --string-list: " << string_list << "\n";
	std::cout << "-i, --int-list: " << int_list << "\n";
	std::cout << "-f, --float-list: " << float_list << "\n";
	std::cout << "-c, --custom-list: " << custom_strings << "\n";

	return 0;
}

