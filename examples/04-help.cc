#include <iostream>

#include "Dashh.hh"

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;

	cmd.setVersion("example-04 0.0.1");

	cmd << Text("example-04 -- A program for reading and writing enties");

	cmd << Text("\nUsage:");
	cmd << Usage("[-v]");
	cmd << Usage("[-v] read [FILE...]");
	cmd << Usage("[-v] write [-o | --output FILE]");

	cmd << "";

	cmd <<
		"Note that this text is automatically filled "
		"to the width of 72 characters, but it is possible to force "
		"a line-break, for example\n<--- here.";

	cmd << "\nOptions:";

	bool verbose = false;
	cmd << Flag(verbose, "-v, --verbose", "Produce verbose output.");
	cmd << Flag(PageHelp(), "-h, --help", "Print help (this message) and exit");
	cmd << Flag(PageVersion(), "--version", "Print version information and exit");

	cmd << "\nCommands:";

	std::string arg_cmd;
	Command cmd_write(arg_cmd, "write", "Writes an entry to specified file");

	cmd_write.setVersion("example-04 write 2.2.1");

	cmd_write << Text("example-04 write -- Writes an entry to specified file");

	cmd_write << Text("\nUsage:");
	cmd_write << Usage("[-v] write [-o | --output FILE]");

	cmd_write << "\nOptions:";

	std::string output;
	cmd_write << Option(output, "-o, --output", "FILE", "Path to the output file");
	cmd_write << Flag(PageHelp(), "-h, --help", "Print help (this message) and exit");
	cmd_write << Flag(PageVersion(), "-v, --version", "Print version information and exit");

	cmd << cmd_write;

	std::vector<std::string> input;
	Command cmd_read(arg_cmd, "read", "Reads entries from specified files");
	cmd_read << Text("example-04 read -- Reads entries from specified files");

	cmd_read << "\nOptions:";
	cmd_read << Positional(input, "FILE...", "Files to read");

	bool flag_1;
	cmd_read << Flag(flag_1, "-f, --flag-1", "Some flag with no puprpose");
	cmd_read << Flag(PageHelp(), "-h, --help", "Print help (this message) and exit");

	cmd << cmd_read;

	cmd << "\nEnvironment variables:";
	cmd << KeyValue("SOME_VAR", "Description for SOME_VAR");
	cmd << KeyValue("ANOTHER_VAR", "Description for ANOTHER_VAR");
	cmd << KeyValue("NOT EVEN A VAR", 
		"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod"
		"tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."
	);

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << std::boolalpha;
	std::cout << "verbose: " << verbose << "\n";
	std::cout << "arg_cmd: " << arg_cmd << "\n";
	std::cout << "output: "  << output  << "\n";
	std::cout << "flag_1: "  << flag_1  << "\n";

	std::cout << "input: ";
	for (auto f : input)
		std::cout << f << ", ";
	std::cout << "\n";

	return 0;
}
