#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "" "$(cat << EOF
08-required: missing '-n' argument!
EOF
)"

cmp "-n" "$(cat << EOF
08-required: option '-n' requires a value!
EOF
)"

cmp "-n 1" "$(cat << EOF
08-required: missing 'V' argument!
EOF
)"

cmp "2" "$(cat << EOF
08-required: missing '-n' argument!
EOF
)"

cmp "-n1 2" "$(cat << EOF
-n: 1
v: 2
EOF
)"
